(function () {
	'use strict'

	angular
		.module('spress')
		.controller('add', ['$scope', '$timeout', '$window', '$routeParams', '$location', 'cmsModel', add]);

	function add($scope, $timeout, $window, $routeParams, $location, cmsModel) {
		$scope.activetab = 'hometab';
		$scope.activetabsub = 'landingtab';

		var cms_model = new cmsModel();
		$scope.info = {
			version: '',
			source: '',
			store_id: '',
			status: '',
		};

		$timeout(function () {
			$scope.page = {
				'Home': {
					'Module': new Array()
				},
				'Women': {
					'Module': new Array()
				},
				'Men': {
					'Module': new Array()
				},
				'Kids': {
					'Module': new Array()
				}
			}


			$scope.tabchangemain = function tabchangemain(activetab) {
				//console.log($scope.activetab);
				$scope.tabsubchange = function tabsubchange(activetabsub, activetab) {
					if ($scope.activetabsub == 'landingtab') {
						//console.log($scope.activetabsub);
						$scope.page = {
							'Home': {
								'Module': new Array()
							},
							'Women': {
								'Module': new Array()
							},
							'Men': {
								'Module': new Array()
							},
							'Kids': {
								'Module': new Array()
							}
						}
						var switchtolisting = $window.confirm('Are you sure ? as You are going to loose all Listing data if you switch to Listing without saving!');
						if (switchtolisting) {
							$window.alert('Going to Switch to Listing');

						}

					} else if ($scope.activetabsub == 'listingtab') {
						console.log($scope.activetabsub);
						$scope.page = {
							'Home': {
								'Module': new Array()
							},
							'Women': {
								'Module': []
							},
							'Men': {
								'Module': []
							},
							'Kids': {
								'Module': []
							}
						}
						var switchtolanding = $window.confirm('Are you sure ? as You are going to loose all Landing data if you switch to Listing without saving!');
						if (switchtolanding) {
							$window.alert('Going to Switch to Landing');

						}
					}
				}
			}
		});

		$scope.tabed = function (tabid) {
			$scope.activetab = tabid;
		}
		$scope.isactive = function (tabid) {
			return ($scope.activetab == tabid);
		}

		$scope.tabedsub = function (tabidsub) {
			$scope.activetabsub = tabidsub;

		}
		$scope.isactivesub = function (tabidsub) {
			return ($scope.activetabsub == tabidsub);
		}

		$scope.save = function () {
			savepage('draft');
		}
		

		function savepage(type) {
			$scope.info.status = type;
			
			cms_model.send = {
				'data_details': angular.toJson($scope.info),
				'data': angular.toJson($scope.page),
			};
			
			cms_model.add().then(function (state) {
				$timeout( function(){
					$location.path('list').search('');
				}, 2000 );
			});
			
		}
	}
})();