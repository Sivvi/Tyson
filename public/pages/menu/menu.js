(function () {
	'use strict'

	angular
		.module('spress')
		.controller('menu', ['$scope', '$timeout', 'cmsModel', menu]);

	function menu($scope, $timeout, cmsModel){
                $scope.menu = new Object();
                $scope.menuitems = new Array();
		var cms_model = new cmsModel();
                
                fetchmenu();
                
                $scope.activemenutab = 'tab-0';

                function fetchmenu(){
                    cms_model.send = {
                        'query': 'status:publish',
                        'params': {
                            'wt': 'json',
                            'start': 0,
                            'rows': 1,
                            'fl': 'store_id, version, status, data',
                            'fsort': 'timestamp desc'
                        },
                        'source': 'mobile',
                        'store_id': 1
                    };
                    cms_model.menu().then(function(data, key){                     
                        $scope.menu = cms_model.record['CategoryTree'];
                        $scope.menuitems = menuitems($scope.menu);
                    });	
		}
              
                
                function menuitems(menu){
                    var result = new Array();
                    for (var key in menu) {
                        if (menu.hasOwnProperty(key)) {
                            result.push({
                                'key': key,
                                'value': menu[key]
                            });
                       console.log($scope.menu[key]);
                        }
                    }
                     
                    return result;
                }
                $scope.tabedmenu = function (tabmenuid) {
			$scope.activemenutab = tabmenuid;
                        
		}
		$scope.isactivemenu = function (tabmenuid) {
			return ($scope.activemenutab == tabmenuid);
//                      console.log($scope.activemenutab); 
		}
                $scope.tabedmenusub = function (tabmenuidsub) {
			$scope.activemenutabsub = tabmenuidsub;
                        console.log(tabmenuidsub);
		}
		$scope.isactivemenusub = function (tabmenuidsub) {
			return ($scope.activemenutabsub == tabmenuidsub);
		}
                
                
               
        }
})();

