(function () {
	'use strict';

	angular
		.module('spress')
		.controller('edit', ['$scope', '$http', '$timeout', '$window', '$location', '$routeParams', 'cmsModel', edit]);

	function edit($scope, $http, $timeout, $window, $location, $routeParams, cmsModel) {
		$scope.activetab = 'hometab';
		$scope.activetabsub = 'landingtab';

		var cms_model = new cmsModel();
		$scope.info = {
			version: '',
			source: '',
			store_id: '',
			status: '',
		};
		$scope.page = {
			'Home': {
				'Module': new Array()
			},
			'Women': {
				'Module': new Array()
			},
			'Men': {
				'Module': new Array()
			},
			'Kids': {
				'Module': new Array()
			}
		};
		$scope.tabchangemain = function tabchangemain(activetab) {
		$scope.tabsubchangeedit = function tabsubchangeedit(activetabsub) {
			if ($scope.activetabsub == 'landingtab') {
				console.log('landing');
				$scope.page = {
					'Home': {
						'Module': new Array()
					},
					'Women': {
						'Module': new Array()
					},
					'Men': {
						'Module': new Array()
					},
					'Kids': {
						'Module': new Array()
					}
				};
				var switchtolisting = $window.confirm('Are you sure ? as You are going to loose all Listing data if you switch to Listing without saving!');
				if (switchtolisting) {
					$window.alert('Going to Switch to Listing');

				}

			} else if ($scope.activetabsub == 'listingtab') {
				console.log('listing');
				$scope.page = {
					'Home': {
						'Module': new Array()
					},
					'Women': {
						'Module': []
					},
					'Men': {
						'Module': []
					},
					'Kids': {
						'Module': []
					}
				};
				var switchtolanding = $window.confirm('Are you sure ? as You are going to loose all Landing data if you switch to Listing without saving!');
				if (switchtolanding) {
					$window.alert('Going to Switch to Landing');

				}
			}
		}
		};


		cms_model.send = {
			'query': 'unique:' + $routeParams.id,
			'params': {
				'wt': 'json',
				'start': 0,
				'rows': 20,
				'fl': 'store_id, source, version, status, data, unique',
				'sort': 'timestamp desc',
			}
		};
		cms_model.search().then(function (state) {
			var record = cms_model.records[0];
			var data = JSON.parse(record.data);

			$timeout(function () {
				$scope.info.source = [record.source];
				$scope.info.version = record.version;
				$scope.info.store_id = [record.store_id];
				$scope.info.status = record.status;

				$scope.page.Home = data.Home;
				$scope.page.Men = data.Men;
				$scope.page.Women = data.Women;

				if (typeof data.Kids === 'undefined') $scope.page.Kids = { 'Module': new Array() };
				else $scope.page.Kids = data.Kids;
			})

		});

		$scope.tabed = function (tabid) {
			$scope.activetab = tabid;
		}

		$scope.isactive = function (tabid) {
			return ($scope.activetab == tabid);
		}

		$scope.tabedsub = function (tabid) {
			$scope.activetabsub = tabid;
		}
		$scope.isactivesub = function (tabid) {
			return ($scope.activetabsub == tabid);
		}


		$scope.save = function (type) {
			if (type == 'publish') {
				if (confirm('Are you sure?')) savepage(type);
			}
			else {
				savepage(type);
			}
		}
		
		function savepage(type) {
			$scope.info.status = type;
			
			cms_model.send = {
				'data_details': angular.toJson($scope.info),
				'data': angular.toJson($scope.page),
			};
			cms_model.add().then(function (state) {
				 $timeout( function(){
					$location.path('list').search('');
			         }, 2000 );
			});
			
		}
	}
})()
