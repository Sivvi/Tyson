(function(){
	'use strict'

	angular
		.module('spress')
		.controller('login', ['$scope', '$routeParams', '$rootScope', '$location', 'authService', 'flashService', login]);

	function login($scope, $routeParams, $rootScope, $location, authService, flashService){
		if(authService.isAuth()){ 
			$location.path('/list');
			return;
		}

		$scope.form = {
			data: new Object(),
			errors: new Object(),
		}

		$scope.submit = function(){
			if(valid($scope.form, 'login')){
				if($scope.form.data.email == 'admin@sivvi.com' && $scope.form.data.password == '123456'){
					authService.setCredentials({
						'username': 'sivviadmin',
						'email': 'admin.sivvi.com',
						'token': '31c89471n938x47927c89qx7x9873n9qw87x19rcy29'
					});
					if(typeof $routeParams.back === 'undefined') $location.path('/list');
					else $location.path($routeParams.back).search('');
				}
				else {
					flashService.error($rootScope, 'Error! incorrect email or password.');
				}
			}
		}

		function valid(form, name){
			form.errors = {'has':{}, 'message':{}};

			//required fields
			angular.forEach(form[name].$error.required, function(field){
				form.errors.has[field.$name] = true;
				form.errors.message[field.$name] = 'Field required';
			});

			//email validation
			angular.forEach(form[name].$error.email, function(field){
				form.errors.has[field.$name] = true;
				form.errors.message[field.$name] = 'Invalid email address (abc@xyz.com)';
			});

			return !form.$invalid;
		}
	}
})();