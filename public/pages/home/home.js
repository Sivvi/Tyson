(function(){
	'use strict'

	angular
		.module('spress')
		.controller('home', ['$scope', '$location', 'authService', home]);

	function home($scope, $location, authService){
		if(!authService.isAuth()){
			$location.url('/login');
			return
		}

		$location.url('/add');
	}
})();