(function(){
	'use strict'

	angular
		.module('spress')
		.controller('list', ['$scope','$window', 'cmsModel', list]);

	function list($scope, $window, cmsModel){
		$scope.records = new Array();
		var cms_model = new cmsModel();
		
		fetchlistings();

		$scope.remove = function(item){
			var id = item.unique;
			cms_model.send = {
				'data': {
					'unique': id
				}
			}
			cms_model.remove().then(function(state){
				fetchlistings();
			});
		}

		function fetchlistings(){
			cms_model.send = {
				'query':'*',
				'params': {
					'wt': 'json',
					'start': 0,
					'rows': 30000,
					'fl': 'store_id, source, version, status, data, unique',
					'sort': 'timestamp desc',
				}
			}
			cms_model.search().then(function(state){
				$scope.records =  cms_model.records;
			});
		}

		$scope.removeUser = function(data) {
			var deleteUser = $window.confirm('Are you absolutely sure you want to delete?');
			if (deleteUser) {
			  $window.alert('Going to delete');
			  $scope.remove(data);
			}
		  }
		}
	
})();




