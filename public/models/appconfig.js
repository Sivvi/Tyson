(function(){
	'use strict'

	angular
		.module('spress')
		.factory('cmsModel', ['$q', '$http', '$cookies', 'constants', 'httpService', 'responseFactory', cmsModel]);

	function cmsModel($q, $http, $cookies, constants, httpService, responseFactory){
		var factory = cms;

		function cms(){
			this.loading = false;
			this.send = new Object();
			this.record = new Object();
			this.records = new Array();
			this.navigation = new Object();
			this.errors = {
				'has': new Object(),
				'message': new Object()
			}

			cms.prototype.search = function(){
				var self = this,
					url = 'sPGetAppConfig',
					deferred = $q.defer();

				$http({
					method: 'POST',
					url: constants.api.path+url,
					transformRequest: function(obj) {
						obj['sessionToken'] = $cookies.getObject('sp-api-session');
				        return serialize(obj);
				    },
					data: self.send
				}).then(function(receive){
					var parse = new responseFactory(receive);
					self.records = parse.records;
					deferred.resolve();
				})
				.catch(function(reason, cause){
					console.error(reason, cause);
				});

				return deferred.promise;
			}
                        
                        cms.prototype.menu = function(){
				var self = this,
                                    deferred = $q.defer();

				$http({
					method: 'POST',
					url: 'https://sap.sivvi.com/catalog-new/v4/content/landing',
					transformRequest: function(obj) {
						obj['sessionToken'] = $cookies.getObject('sp-api-session');
				        return serialize(obj);
				    },
					data: self.send
				}).then(function(receive){
//					var parse = new responseFactory(receive);
					self.record = receive['data']['result'];
					deferred.resolve();
				})
				.catch(function(reason, cause){
					console.error(reason, cause);
				});

				return deferred.promise;
			}

			cms.prototype.remove = function(){
				var self = this,
					url = 'sPDeleteAppConfig',
					deferred = $q.defer();

				$http({
					method: 'POST',
					url: constants.api.path+url,
					transformRequest: function(obj) {
						obj['sessionToken'] = $cookies.getObject('sp-api-session');
				        return serialize(obj);
				    },
					data: self.send
				}).then(function(receive){
					deferred.resolve();
				})
				.catch(function(reason, cause){
					console.error(reason, cause);
				});

				return deferred.promise;
			}

			cms.prototype.add = function(){
				var self = this,
					//url = 'sPSaveAppConfig',
					url = 'sPSaveAppConfigBulk',
					deferred = $q.defer();

				$http({
					method: 'POST',
					url: constants.api.path+url,
					transformRequest: function(obj) {
						obj['sessionToken'] = $cookies.getObject('sp-api-session');
				        return serialize(obj);
				    },
					data: self.send
				}).then(function(receive){
					deferred.resolve();
				})
				.catch(function(reason, cause){
					console.error(reason, cause);
				});

				return deferred.promise;
			}
		}


		function serialize(obj, prefix) {
			var str = [], p;
			for(p in obj) {
				if (obj.hasOwnProperty(p)) {
					var k = prefix ? prefix + "[" + p + "]" : p, v = obj[p];
					str.push((v !== null && typeof v === "object") ? serialize(v, k) : encodeURIComponent(k) + "=" + encodeURIComponent(v));
				}
			}
			return str.join("&");
		}

		return factory;
	}
})()