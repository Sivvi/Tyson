(function(){
	'use strict';

	angular
		.module('spress')
		.service('apiSession', ['$http', '$cookies', 'constants', apiSession]);

	function apiSession($http, $cookies, constants){
		var self = this;

		self.getaccess = function(){
			var send = {
				'username': constants.api.username,
        		'password': constants.api.password,
			}
			$http({ 
				method: 'POST', 
				url: constants.api.path+'sPAuthenticate',
				data: send,
			})
			.then(function(receive){
				$cookies.putObject('sp-api-session', receive.data.result.sessionToken);
			})
			.catch(function(reason, cause){
				console.error(reason, cause);
			});
		}
	}
})();