(function(){
	'use strict'

	angular
		.module('spress', [
			'ngRoute', 
			'ngCookies', 
			'FlashModule', 
			'AuthModule', 
			'HttpModule', 
			'ResponseModule',
			'ClickcopyModule',
			'angular-loading-bar'])
		.run(['$rootScope', '$http', '$location', '$cookies', 'authService', 'apiSession', run])
		.config(['$routeProvider', '$locationProvider', config])
		.filter('store', function(){
			return function(input) {
				var result = input;

				switch(input){
					case 1:
				        result = 'UAE English';
				    break;
				    case 2:
				        result = 'UAE Arabic';
				    break;
				    case 3:
				        result = 'KSA English';
				    break;
				    case 4:
				        result = 'KSA Arabic';
				    break;
				    case 5:
				        result = 'Oman English';
				    break;
				    case 6:
				        result = 'Oman Arabic';
				    break;
				    case 7:
				        result = 'Bahrain English';
				    break;
				    case 8:
				        result = 'Bahrain Arabic';
				    break;
				    case 9:
				        result = 'Kuwait English';
				    break;
				    case 10:
				        result = 'Kuwait Arabic';
				    break;
				    case 11:
				        result = 'Qatar English';
				    break;
				    case 12:
				        result = 'Qatar Arabic';
				    break;
				    case 13:
				        result = 'Intl English';
				    break;
				    case 14:
				        result = 'Intl Arabic';
				    break;
				}
				return result;
			}
		})
		.filter('source', function(){
			return function(input) {
				var result = input;

				switch(input){
					case 'ios':
				        result = 'iOS';
				    break;
				    case 'android':
				        result = 'Android';
				    break;
				    case 'mobile':
				        result = 'Mobile';
				    break;
				}
				return result;
			}
		})
		.factory('constants',[constants]);

	function constants(){
        var factory = new Object();
        factory.directives = 'directives/';
        factory.api = {
        	'path':'https://spress-app.sivvi.com/v1/',
        	 //'path':'https://spress.sivvi.com/v1/',
        	 //'path':'http://localhost:8009/v1/',
        	'username': 'sivvi',
        	'password': 'N0*6%XEwb:!2Y|m'
        }
        return factory;
    }

	function run($rootScope, $http, $location, $cookies, authService, apiSession){
		$rootScope.globals = {
			user_auth: false,
			currentUser: {}
		};	

		apiSession.getaccess();

		if(authService.isAuth()){
			$rootScope.globals.user_auth = true;
			$rootScope.globals.currentUser = authService.user();
			$http.defaults.headers.common['Authorization'] = $rootScope.globals.currentUser.token;
		}
		$http.defaults.headers.common['Content-type'] = 'application/x-www-form-urlencoded';
		
		$rootScope.$on("$routeChangeStart", function(evt, to, from) {
			if(typeof to === 'undefined') return;
			if(to.verifyAuth === true){
				to.resolve = to.resolve || {};
				if (!to.resolve.authenticationResolver){
					to.resolve.authenticationResolver = ['$q', '$location', 'authService', function($q, $location, authService) {
						var deferred = $q.defer();
						if(!authService.isAuth()){
							deferred.reject({'back': $location.path()});
						}
						else{
							deferred.resolve();
						}

						return deferred.promise;
					}];
				}
			}
		});

		$rootScope.$on("$routeChangeError", function(evt, to, from, back){
			$location.path('/').search(back);
        });
	}

	function config($routeProvider, $locationProvider){
		$routeProvider
			.when('/', {
				controller: 'login',
				templateUrl: 'pages/login/login.html',
			})
			// .when('/login', {
			// 	controller: 'login',
			// 	templateUrl: 'pages/login/login.html',
			// })
			.when('/add', {
				controller: 'add',
				templateUrl: 'pages/add/add.html',
				verifyAuth: true
			})
                        .when('/menu', {
				controller: 'menu',
				templateUrl: 'pages/menu/menu.html',
				verifyAuth: true
			})
			.when('/edit/:id', {
				controller: 'edit',
				templateUrl: 'pages/edit/edit.html',
				verifyAuth: true
			})
			.when('/list', {
				controller: 'list',
				templateUrl: 'pages/list/list.html',
				verifyAuth: true
			});
		$locationProvider.html5Mode(true);
	}
	

	Object.defineProperty(Object.prototype, 'fishout', {
		value: function(properties){
			if (this === undefined || this === null){ 
				return;
			}
			if (properties.length === 0){ 
				return this.valueOf();
			}

			if(properties[0].split(':').length == 2){
				var key = properties[0].split(':')[0];
				var element = properties[0].split(':')[1];
				var foundSoFar = this[key][element];
			}
			else{
				var foundSoFar = this[properties[0]];
			}
			var remainingProperties = properties.slice(1);
			if(typeof foundSoFar !== 'undefined' && foundSoFar !== null) return foundSoFar.fishout(remainingProperties);	
			else return;
		}
	});

})();