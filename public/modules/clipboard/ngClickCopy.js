(function () {
    'use strict'

    angular
        .module('ClickcopyModule', [])
        .service('ngClickCopy', [ngClickCopy]);

    function ngClickCopy($timeout) {
        var copydata;
        var newObject;
        var self = this;

        self.copy = function (data) {
            copydata = Object.assign({}, data);
            newObject = angular.copy(copydata);
           //copydata = self.somefunction(data);
           // console.log(newObject);
        }

        self.paste = function () {
            return newObject;
        }

    }
})()