(function(){
	'use strict'

	angular
		.module('AuthModule',[])
		.service('authService', ['$http', '$rootScope', '$cookies', '$location', authService]);

	function authService($http, $rootScope, $cookies, $location){
		var self = this;

		/**
		 * Store user auth data in cookie (localstorage)
		 * @param {Object} authcred user auth data
		 */
		self.setCredentials = function(authcred, remember){
			var expireDate = new Date();
                expireDate.setDate(expireDate.getDate() + 7);

            var options = { 'expires': (remember) ? expireDate : null }

			$rootScope.globals.user_auth = true;
			$rootScope.globals.currentUser = authcred;
			$cookies.putObject('sp-session', authcred, options);
			$http.defaults.headers.common['Authorization'] = authcred.token;
		}

		/**
		 * Unset user auth credentials
		 * @return {[type]} [description]
		 */
		self.clearCredentials = function(){
			$cookies.remove('sp-session');
			delete $http.defaults.headers.common.Authorization;
			delete $rootScope.globals.user_auth;
			delete $rootScope.globals.currentUser;
		}

		/**
		 * Check access of a user against ACL for a specific URL
		 * @param  {String} url    current browswe url
		 * @param  {String} userId current user
		 * @return {Boolean}        true/false
		 */
		self.checkAccess = function(url, userId){}

		/**
		 * Returns current user ID
		 * @return {String} current user id
		 */
		self.userId = function(){
			var session = $cookies.getObject('sp-session');

			var userdata = session;
			if(typeof userdata === 'undefined' || userdata == null) return;
			else return userdata.id;	
		}

		/**
		 * Returns current username
		 * @return {String} current username
		 */
		self.userName = function(){
			var session = $cookies.getObject('sp-session');
			var userdata = session;
			if(typeof userdata === 'undefined' || userdata == null) return;
			else return userdata.name;
		}

		/**
		 * Returns user complete data
		 * @return {object} User data (id, username, token)
		 */
		self.user = function(){
			var session = $cookies.getObject('sp-session');
			var userdata = session;
			if(typeof userdata === 'undefined' || userdata == null) return;
			else return userdata;
		}

		/**
		 * Check if user is authorized
		 * @return {Boolean} true/false
		 */
		self.isAuth = function(){
			if(typeof self.user() === 'undefined') return false;
			else return true;				
		}

		/**
		 * Check user authentication and redirect user to login page
		 * @param  {String} url Login page URL
		 */
		self.requiredLogin = function(url){
			if(!self.isAuth()) $location.path(url).search('');
		}
	}
})()