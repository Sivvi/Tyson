(function () {
	"use strict";

	angular
		.module('FlashModule', [])
		.service('flashService', ['$timeout', flashService])

    function flashService($timeout) {
        var self = this;

        /**
         * Set flash data in parent scope to show success flash massge
         * @param  {Object} scope           parent scope
         * @param  {string} title           title message
         * @param  {Object} errorList       errors list
         * @return {Object}
         */
        self.success = function(scope, title, errorList) {
            scope.flash = {
                message: title,
                list: (typeof errorList !== 'undefined') ? modifyErrorsList(errorList) : false,
                type: 'success'
            };
            return scope;
        };

        /**
         * Set flash data in parent scope to show error flash massge
         * @param  {Object} scope           parent scope
         * @param  {string} title           title message
         * @param  {Object} errorList       errors list
         * @return {Object}
         */
        self.error = function(scope, title, errorList) {
            scope.flash = {
                message: title,
                list: (typeof errorList !== 'undefined') ? modifyErrorsList(errorList) : false,
                type: 'error'
            };
            return scope;
        };


        /**
         * Set flash data in parent scope to show info flash massge
         * @param  {Object} scope           parent scope
         * @param  {string} title           title message
         * @param  {Object} errorList       errors list
         * @return {Object}
         */
        self.info = function(scope, title, errorList) {
            scope.flash = {
                message: title,
                list: (typeof errorList !== 'undefined') ? modifyErrorsList(errorList) : false,
                type: 'info'
            };
            return scope;
        };


        /**
         * Set flash data in parent scope to show warning flash massge
         * @param  {Object} scope           parent scope
         * @param  {string} title           title message
         * @param  {Object} errorList       errors list
         * @return {Object}
         */
        self.warning = function(scope, title, errorList) {
            scope.flash = {
                message: title,
                list: (typeof errorList !== 'undefined') ? modifyErrorsList(errorList) : false,
                type: 'warning'
            };
            return scope;
        };

        function paramsError(){
            console.error('Parameter missing');
        }

        function modifyErrorsList(list) {
            var array = new Array();
            for (var key in list.has) {
                if (list.has.hasOwnProperty(key)) {
                    array.push({
                        'field': key, 
                        'message': list.message[key]
                    });
                }
            }
            return array;
        }
    }
})();