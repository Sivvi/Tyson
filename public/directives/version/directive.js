(function(){
	'use strict';

	angular
		.module('spress')
		.directive('styleversion', ['$timeout', '$filter', styleversion]);

	function styleversion($timeout, $filter){
		var directive = new Object();
			directive.restrict = 'E';
			directive.scope = {
				dataobject: '='
			};
			directive.template = template;
			directive.link = link;

		function template(element, attributes){
			return '<div class="versionbox">'+
					'<input type="text" class="versionbox_input sp_m0 sp_br4" ng-model="dataobject">'+
					'</div>';
		}

		function link(scope, element, attributes){
			$timeout(function(){
				scope.dataobject = 'Version-'+$filter('date')(+ new Date(),'dd-MM-yyyy');
			})
		}

		return directive;
	}
})();