(function () {
    'use strict'

    angular
        .module('spress')
        .directive('addimage', ['$timeout', addimage]);

    function addimage($timeout) {
        var directive = new Object();
        directive.restrict = "E";
        directive.scope = {
            imagedata: '=',
        };
        directive.link = link;
        directive.templateUrl = templateUrl;

        function templateUrl(element, attributes) {
            return attributes.templateUrl;
        }

        function link(scope, element, attributes) {
            scope.data = new Object();
            scope.formdata = new Object();

            scope.adddataimage = function () {
                scope.pop = true;
                // $('body').addClass('noscroll');
            }
            scope.save = function () {
                var formd = Object.assign({}, scope.formdata);
                formd['LandingData'] = new Array();
                console.log(scope.imagedata);
                scope.imagedata.push(formd);
                scope.pop = false;
                scope.formdata = new Object();
            }

        }



        return directive;
    }

})()