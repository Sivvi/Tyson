(function(){
	'use strict'

	angular
		.module('spress')
		.directive('listmodules', ['$parse', '$timeout', 'constants', listmodules]);

	function listmodules($parse, $timeout, constants){
		var directive = new Object();
			directive.restrict = 'E';
			directive.scope = {
				list: '=',
			};
			directive.link = link;
			directive.templateUrl = templateUrl;

		function templateUrl(element, attributes){
			return attributes.templateUrl;
		}

		function link(scope, element, attributes){
			scope.items = new Array();

			scope.$watch('list', function(newValue, oldValue){
				if(typeof newValue !== 'undefined'){
					scope.items = newValue;
				}
			},true);

			scope.delete = function(index){
				scope.list = deleteModule(index, scope.list);
			}

			scope.saveBlock = function(module_index, column_data, column_id){
				if(column_id != null)scope.list[module_index].Columns[column_id] = column_data;
				else scope.list[module_index].Columns.push(column_data);
			}


			scope.sortDown = function(index){
				var from = index, to = index+1, max = scope.list.length;
				if(to < max){ 
					scope.list.splice(to, 0, scope.list.splice(from, 1)[0]);
				}

			}

			scope.sortUp = function(index){
				var from = index, to = index-1;
				if(to >= 0){ 
					scope.list.splice(to, 0, scope.list.splice(from, 1)[0]);
				}
			}

			scope.viewBlock = function(module_index, column, column_index){
				scope.poptrigger(module_index, column, column_index);
			}

			function arrayMove(from, to, array){
				return array.splice(to, 0, this.splice(from, 1)[0]);
			}

			function findModule(index){
				var result = new Object();
				angular.forEach(scope.list, function(value, ind){
					result = value;
				});
				return result;
			}

			function deleteModule(index, list){
				var DelateDatamodule = window.confirm('Are you absolutely sure you want to delete this Module?');
				if (DelateDatamodule) {
					window.alert('Going to delete');
					var count = 0, result = new Array();
				    angular.forEach(list, function(value, ind){
					if(index != ind) {
						result.push(value);
						count++;
					}
				});
				return result;
				}				
			}
		}
	 return directive;
	}
})();