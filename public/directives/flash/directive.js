(function () {
	"use strict";

	angular
		.module('spress')
		.directive('flashDirective', ['$rootScope', '$parse', '$compile', '$timeout', 'constants', flashDirective]);

    function flashDirective($rootScope, $parse, $compile, $timeout, constants){
		var directive = {}
			directive.restrict= 'EA';
			directive.templateUrl = templateUrl;
			directive.scope = true;
			directive.link = link;

		function templateUrl(element, attributes){
			return constants.directives+'flash/flash.html';
		}
		
		function link(scope, element, attributes){
			var timepromise;
			element[0].addEventListener('click', function(){
				$timeout(function(){
					$timeout.cancel(timepromise);
					delete scope['_data'];
				});
			});
			scope.$watch(watch, function(newValue, oldValue){
				if(newValue !== oldValue){ 
					scope._data = newValue;
					timepromise = $timeout(function(){
						delete scope['_data'];
					},5000);
				}
			});
			function watch(){
				return $parse(attributes.flashData)(scope);
			}
		}
		return directive;
    }
})();