(function () {
    'use strict'

    angular
        .module('spress')
        .directive('editimage', ['$timeout', editimage]);

    function editimage($timeout) {
        var directive = new Object();
        directive.restrict = "E";
        directive.scope = {
            dataobject: '='
        };
        directive.link = link;
        directive.templateUrl = templateUrl;

        function templateUrl(element, attributes) {
            return attributes.templateUrl;
        }
        function link(scope, element, attributes) {
            scope.formdata = scope.dataobject;
            scope.showform = false;

            
            scope.saveedit = function () {
                var formd = Object.assign({}, scope.formdata);
                scope.dataobject = formd;
                scope.showform = false;
                // formd['LandingData'] = new Array();
                // console.log(scope.imagedata);
                // scope.imagedata.push(formd);
                // scope.formdata = new Object();
            }
            
        }



        return directive;
    }

})()