(function(){
	'use strict';

	angular
		.module('spress')
		.directive('source', [source]);

	function source(){
		var directive = new Object();
			directive.restrict = 'E';
			directive.scope = {
				 dataobject: '='
			};
			directive.link = link;
			directive.template = template;

		function template(element, attributes){
			return '<div class="sourcebox" clickintent="pagesource">'+
						'<span class="sourcebox_title sp_br4 pagesource">Source</span>'+
						'<ul class="sourcebox_list">'+
							'<li class="sourcebox_item">'+
								'<input type="checkbox" name="source" checklist-model="dataobject" value="ios" checklist-id="source_mw" class="sourcebox_check">'+
								'<label for="source_mw" class="sourcebox_label">iOS</label>'+
							'</li>'+
							'<li class="sourcebox_item">'+
								'<input type="checkbox" name="source" checklist-model="dataobject" value="android" checklist-id="source_ma" class="sourcebox_check">'+
								'<label for="source_ma" class="sourcebox_label">Android</label>'+
							'</li>'+
							'<li class="sourcebox_item">'+
								'<input type="checkbox" name="source" checklist-model="dataobject" value="mobile" checklist-id="source_dk" class="sourcebox_check">'+
								'<label for="source_dk" class="sourcebox_label">Mobile</label>'+
							'</li>'+
							'<li class="sourcebox_item">'+
								'<input type="checkbox" name="source" checklist-model="dataobject" value="desktop" checklist-id="source_dsk" class="sourcebox_check">'+
								'<label for="source_dsk" class="sourcebox_label">Desktop</label>'+
							'</li>'+
						'</ul>'+
					'</div>';
		}

		function link(scope, element, attributes){}
		
		return directive;
	}
})();