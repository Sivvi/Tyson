(function(){
	'use strict';

	angular
		.module('spress')
		.directive('pageinfo', [pageinfo]);

	function pageinfo(){
		var directive = new Object();
			directive.restrict = 'E';
			directive.scope = {
				dataobject: '='
			};
			directive.template = template;
			directive.link = link;

		function template(element, attributes){
			return '<div class="pageinfo sp_mb20">'+
						'<div class="sp_row stretch">'+
							'<div class="sp_small_4 sp_columns">'+
								'<input type="text" placeholder="id" ng-model="dataobject.Id" class="sp_input sp_br4 sp_m0 pageinfo_id">'+
							'</div>'+
							'<div class="sp_small_10 sp_columns">'+
								'<input type="text" placeholder="title" ng-model="dataobject.Title" class="sp_input sp_br4 sp_m0 pageinfo_title">'+
							'</div>'+
							'<div class="sp_small_10 sp_columns">'+
								'<input type="text" placeholder="url" ng-model="dataobject.Uri" class="sp_input sp_br4 sp_m0 pageinfo_url">'+
							'</div>'+
						'</div>'+
					'</div>';
		}

		function link(scope, element, attributes){}

		return directive;
	}
})()