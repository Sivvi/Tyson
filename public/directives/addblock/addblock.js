(function(){
	'use strict';

	angular
		.module('spress')
		.directive('addblock', ['$q', 'constants', addblock]);

	function addblock($q, constants){
		var directive = new Object();
			directive.restrict = 'E';
			directive.scope = {
				//dataobject: '=',
				trigger: '=',
				save: '&'
			};
			directive.templateUrl = templateUrl;
			directive.link = link;

		function templateUrl(element, attributes){
			return constants.directives+'addblock/view.html';
		}

		function link(scope, element, attributes){
			scope.templates = [
				{'id': 0,'label': 'SubCategory'},
				{'id': 1,'label': 'Products'},
				{'id': 2,'label': 'Sports'},
				{'id': 3,'label': 'Brands'},
				{'id': 4,'label': 'Campaign'},
				{'id': 5,'label': 'GiftGuide'},
				{'id': 6,'label': 'Celebrities'},
				{'id': 7,'label': 'HTML'},
				{'id': 8,'label': 'Landing'},
				{'id': 9,'label': 'SportsList'},
			]
			scope.showpop = false;
			scope.update = false;
			scope.blockdata = new Object();
			scope.currentmodule = null;
			scope.column_id = null;
			
			// scope.$watch('dataobject', function(newValue, oldValue){
			// 	if(typeof newValue !== 'undefined'){
			// 		console.log(newValue);
			// 	}
			// });
			// scope.$watch('data', function(newValue, oldValue){
			// 	console.log(newValue);
			// 	if(typeof newValue !== 'undefined'){
			// 		scope.blockdata = newValue;
			// 	}
			// });

			scope.trigger = function(module_index, column_data, column_id){
				currentmodule = module_index;
				if(typeof column_data !== 'undefined'){ 
					scope.column_id = column_id;
					scope.update = true;
					scope.blockdata = column_data;
				}
				scope.showpop = true;
			}

			scope.closepop = function(){
				reset();
				scope.showpop = false;
			}

			scope.donepop = function(){
				if(typeof scope.blockdata.LandingData === 'undefined') scope.blockdata['LandingData'] = new Array();
				scope.blockdata['Template'] = parseInt(scope.blockdata.Template);
				scope.save({'module_index': currentmodule, 'column_data': scope.blockdata, 'column_id': scope.column_id});
				reset();
				scope.showpop = false;
			}

			function reset(){
				scope.column_id = null;
				scope.update = false;
				scope.blockdata = new Object();
			}
		}

		return directive;
	}
})()