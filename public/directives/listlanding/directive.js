(function(){
	'use strict';

	angular
		.module('spress')
		.directive('listlanding', [listlanding]);

	function listlanding(){
		var directive = new Object();
			directive.restrict = 'E';
			directive.scope = {
				'dataobject': '<'
			};
			directive.templateUrl = templateUrl;
			directive.link = link;

		function templateUrl(element, attributes){
			return attributes.templateUrl;
		}

		function link(scope, element, attributes){
			scope.items = new Array();
			scope.$watch('dataobject', function(newValue, oldValue){
				if(typeof newValue !== 'undefined'){
					scope.items = newValue;
				}
				
			});
		}
		return directive;
	}
})()