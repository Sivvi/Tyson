(function(){
	'use strict';

	angular
		.module('spress')
		.directive('aspect', ['$q', aspect]);

	function aspect($q){
		var directive = new Object();
			directive.restrict = 'A';
			directive.scope = {
				'aspect': '<',
				'ngModel': '='
			};
			directive.link = link;

		function link(scope, element, attributes){
			scope.$watch('aspect', function(newValue, oldValue){
				if(typeof newValue !== 'undefined'){
					var columns = newValue.length;
					if(columns != 0 && typeof newValue[0] !== 'undefined'){
						imageSize(newValue[0].Image).then(function(result){
							scope.ngModel = (result.height/result.width)/newValue.length;
						});
					}
				}
				
			}, true);

			function imageSize(src){
				var deferred = $q.defer();

				var result = new Object(), image = new Image();
				image.src = src;
				image.onload = function() {
					result['width'] = image.naturalWidth;
					result['height'] = image.naturalHeight;
					deferred.resolve(result);
				}

				return deferred.promise;
			}
			
		}

		return directive;
	}
})()