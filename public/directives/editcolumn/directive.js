(function(){
	'use strict';

	angular
		.module('spress')
		.directive('editcolumn', ['$timeout', editcolumn]);

	function editcolumn($timeout){
		var directive = new Object();
			directive.restrict = 'E';
			directive.scope = {
				dataobject: '=',
			};
			directive.link = link;
			directive.templateUrl = templateUrl;

		function templateUrl(element, attributes){
			return attributes.templateUrl;
		}
		

		function link(scope, element, attributes, $timeout){

			console.log(scope.dataobject);

			//var scope_data = new Object();
			scope.pop = false;
			scope.data = new Object();
			scope.images = scope.dataobject.images;			

			scope.showpop = function(){
				$('body').addClass('noscroll');
				scope.data = scope.dataobject;
				scope.pop = true;
				
			}
			scope.tabcontroller = function(){
				scope.tab = 1;

				scope.setTab = function (tabId) {
					scope.tab = tabId;
				};
		
				scope.isSet = function (tabId) {
					return scope.tab === tabId;
				};
			}

			scope.closepop = function(){
				$('body').removeClass('noscroll');
				scope.pop = false;
			}

			scope.save = function(){
				scope.data['images'] = scope.images;
				$('body').removeClass('noscroll');
				scope.pop = false;
				// scope.dataobject.Columns.push(scope.data);
				// console.log(scope.dataobject);
			}
			

			// function shiftdata(bin, data){
			// 	for (var key in data) {
			// 		if (data.hasOwnProperty(key)) {
			// 			bin[key] = data[key];
			// 		}
			// 	}
			// }
		}
	
		return directive;
	}

})()