(function () {
    'use strict'

    angular
        .module('spress')
        .directive('addmenuimage', ['$timeout', addmenuimage]);

    function addmenuimage($timeout) {
        var directive = new Object();
        directive.restrict = "E";
        directive.scope = {
            menuitems: '=',
        };
        directive.link = link;
        directive.templateUrl = templateUrl;

        function templateUrl(element, attributes) {
            return attributes.templateUrl;
        }

        function link(scope, element, attributes) {
            scope.data = new Object();
            scope.formdatamenu = new Object();
            scope.menuitems['MenuImageData'] = new Array();

            scope.addmenuimage = function () {
                scope.popmenu = true;
            }
            scope.savemenudata = function () {
               var formdmenu = Object.assign({}, scope.formdatamenu);
//               console.log(scope.menuitems);
               scope.menuitems.MenuImageData.push(formdmenu);
               scope.popmenu = false;
               scope.formdatamenu = new Object();
            }

        }
     return directive;
    }

})()