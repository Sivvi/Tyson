(function(){
	'use strict';

	angular
		.module('spress')
		.directive('sortable', [sortable]);

	function sortable(){
		var directive = new Object();
			directive.restrict = 'A';
			directive.scope = {
				'sortable': '&',
				'sortaxis': '<',
			};
			directive.link = link;

		function link(scope, element, attributes){
			//console.log(element[0]);
			$(element[0]).sortable({
				axis: scope.sortaxis,
				handle: '.sort',
				placeholder: 'ui-state-highlight',
				start: function(event, ui){
					var placeholder_height = ui.item.height();
					$('.ui-sortable-placeholder').height(placeholder_height+1);
				},
				update: function(event, ui){
					var from = $(ui.item).data('index');
					var to = $(ui.item).index();
					scope.sortable({'from': from, 'to': to});
				}
			});
		}
		return directive;
	}
})()