(function(){
	'use strict'

	angular
		.module('spress')
		.directive('logout', ['$location', '$timeout', 'authService', logout]);

	function logout($location, $timeout, authService){
		var directive = new Object();
			directive.restrict = 'A';
			directive.scope = true;
			directive.link = link;

		function link(scope, element, attributes){
			var elem = element[0];

			elem.addEventListener('click', function(event){
				event.preventDefault();
				$timeout(function(){
					authService.clearCredentials();
					$location.path('/').search('');
				});
			});
		}

		return directive;
	}
})();