(function(){
	'use strict'

	angular
		.module('spress')
		.directive('addcolumn', ['$timeout', addcolumn]);

	function addcolumn($timeout){
		var directive = new Object();
			directive.restrict = 'E';
			directive.scope = {
				dataobject: '=',
			};
			directive.link = link;
			directive.templateUrl = templateUrl;

		function templateUrl(element, attributes){
			return attributes.templateUrl;
		}

		function link(scope, element, attributes){
			
			scope.pop = false;
			scope.data = new Object();
			scope.images = new Array();
			// scope.templates = [
			// 	{'id': 0,'label': 'SubCategory'},
			// 	{'id': 1,'label': 'Products'},
			// 	{'id': 2,'label': 'Sports'},
			// 	{'id': 3,'label': 'Brands'},
			// 	{'id': 4,'label': 'Campaign'},
			// 	{'id': 5,'label': 'GiftGuide'},
			// 	{'id': 6,'label': 'Celebrities'},
			// 	{'id': 7,'label': 'HTML'},
			// 	{'id': 8,'label': 'Landing'},
			// 	{'id': 9,'label': 'SportsList'},
			// ];

			scope.showpop = function(){
				reset();
				scope.pop = true;
				// $('body').addClass('noscroll');
			}
			

			scope.closepop = function(){
				reset();
				// $('body').removeClass('noscroll');
				scope.pop = false;
			}

			// scope.imageSave = function(data){
			// 	// scope.data['LandingData'] = new Array();
			// 	scope.dataobject.Columns.push(data);
			// 	// console.log(data);
			// }

			scope.save = function(){
				scope.data['images'] = scope.images;
				scope.dataobject.Columns.push(scope.data);

			// 	$('body').removeClass('noscroll');
			// 	scope.pop = false;
			}

			function reset(){
				scope.data = new Object();
				scope.images = new Array();
			}
		}

		return directive;
	}
})()