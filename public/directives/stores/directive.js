(function(){
	'use strict';

	angular
		.module('spress')
		.directive('stores', ['$timeout', stores]);

	function stores($timeout){
		var directive = new Object();
			directive.restrict = 'E';
			directive.scope = {
				dataobject: '='
			};
			directive.link = link;
			directive.template = template;

		function template(element, attributes){
			return '<div class="storebox" clickintent="pagestore">'+
						'<span class="storebox_title sp_br4 pagestore">Store</span>'+
						'<ul class="storebox_list">'+
						////////////////// Commenting Multiselct checkbox group Start
						// '<li class="storebox_item select-all-check">'+
						//     '<input type="checkbox" name="stores" ng-change="checkAll(all)" ng-model="selectedAll" class="storebox_check">'+
						//     '<label for="" class="storebox_label">Select All</label>'+
						// '</li>'+
						// '<li class="storebox_item select-int">'+
						//     '<input type="checkbox" name="stores" ng-change="checkAllInt(allInt)" ng-model="selectedAllInt" class="storebox_check">'+
						//     '<label for="" class="storebox_label">Select All Int</label>'+
						// '</li>'+
						// '<li class="storebox_item select-gcc-en">'+
						// 	'<input type="checkbox" name="stores" ng-change="checkGccEn(allGccEn)" ng-model="selectedGccEn" class="storebox_check">'+
						// 	'<label for="" class="storebox_label">Select All GCC En</label>'+
						// '</li>'+
						// '<li class="storebox_item select-gcc-ar">'+
						// 	'<input type="checkbox" name="stores" ng-change="checkGccAr(allGccAr)" ng-model="selectedGccAr"class="storebox_check">'+
						// 	'<label for="" class="storebox_label">Select All GCC Ar</label>'+
						// '</li>'+
						// '<li class="storebox_item select-all-me-en">'+
						// 	'<input type="checkbox" name="stores" ng-change="checkUAEKSAEn(allUAEKSAEn)" ng-model="selectedUAEKSAEn" class="storebox_check">'+
						// 	'<label for="" class="storebox_label">Select UAE & KSA En</label>'+
						// '</li>'+
						// '<li class="storebox_item select-all-me-ar">'+
						// 	'<input type="checkbox" name="stores" ng-change="checkUAEKSAAr(allUAEKSAAr)" ng-model="selectedUAEKSAAr" class="storebox_check">'+
						// 	'<label for="" class="storebox_label">Select UAE & KSA Ar</label>'+
						// '</li>'+
						////////////////// Commenting Multiselct checkbox group End
							'<li class="storebox_item" ng-repeat="store in stores">'+
								//'<input type="radio" name="stores" ng-model="dataobject.store_id" value="{{store.value}}" id="{{store.id}}" class="storebox_check">'+
							    '<input type="checkbox" name="stores" checklist-model="dataobject.store_id" checklist-value="store.value"  class="storebox_check">'+
								'<label for="{{store.id}}" class="storebox_label">{{store.title}}</label>'+
							'</li>'+
						'</ul>'+
					'</div>'		
		}
		
		


		function link(scope, element, attributes){
			
			scope.stores = [
			    {
			        'id': 'store_ae_english',
			        'title': 'UAE English',
			        'value': '1',
			    },
			    {
			        'id': 'store_ae_arabic',
			        'title': 'UAE Arabic',
			        'value': '2',
			    },
			    {
			        'id': 'store_sa_english',
			        'title': 'KSA English',
			        'value': '3',
			    },
			    {
			        'id': 'store_sa_arabic',
			        'title': 'KSA Arabic',
			        'value': '4',
			    },
			    {
			        'id': 'store_om_english',
			        'title': 'Oman English',
			        'value': '5',
			    },
			    {
			        'id': 'store_om_arabic',
			        'title': 'Oman Arabic',
			        'value': '6',
			    },
			    {
			        'id': 'store_bh_english',
			        'title': 'Bahrain English',
			        'value': '7',
			    },
			    {
			        'id': 'store_bh_arabic',
			        'title': 'Bahrain Arabic',
			        'value': '8',
			    },
			    {
			        'id': 'store_kw_english',
			        'title': 'Kuwait English',
			        'value': '9',
			    },
			    {
			        'id': 'store_kw_arabic',
			        'title': 'Kuwait Arabic',
			        'value': '10',
			    },
			    {
			        'id': 'store_qa_english',
			        'title': 'Qatar English',
			        'value': '11',
			    },
			    {
			        'id': 'store_qa_arabic',
			        'title': 'Qatar Arabic',
			        'value': '12',
			    },
			    {
			        'id': 'store_int_english',
			        'title': 'Intl English',
					'value': '13',
			    },
			    {
			        'id': 'store_int_arabic',
			        'title': 'Intl Arabic',
			        'value': '14',
			    },
			];
			
			
			
			
////////////////// Commenting Multiselct checkbox group Start
			// scope.checkAll = function() {
			// 	if (scope.selectedAll) {
			// 		scope.selectedAll = true;
			// 		scope.dataobject.store_id = scope.list.checklist;
			// 		scope.list.checklist.push("1","2","3","4","5","6","7","8","9","10","11","12","13","14");
					
			// 	} else {
			// 		scope.selectedAll = false;
			// 		scope.selectedAllInt = false;
			// 		scope.selectedGccEn = false;
			// 		scope.selectedGccAr = false;
			// 		scope.selectedUAEKSAEn = false;
			// 		scope.selectedUAEKSAAr = false;
			// 		scope.dataobject.store_id = [];
			// 		scope.list.checklist = [];
					
			// 	}
			// };

			// scope.checkAllInt = function() {
			// 	if (scope.selectedAllInt) {
			// 		scope.selectedAllInt = true;
			// 		scope.dataobject.store_id = scope.list.checklist;
			// 		scope.list.checklist.push("13","14");
					
			// 	} else {
			// 		scope.selectedAllInt = false;
			// 	    scope.dataobject.store_id = [];
			// 	}
			// };

			// scope.checkGccEn = function() {
			// 	if (scope.selectedGccEn) {
			// 		scope.selectedGccEn = true;
			// 		scope.dataobject.store_id = scope.list.checklist;
			// 		scope.list.checklist.push("5", "7", "9", "11");
				
			// 	} else {
			// 		scope.selectedGccEn = false;
			// 		scope.dataobject.store_id = [];
			// 	}
			// };
			// scope.checkGccAr = function() {
			// 	if (scope.selectedGccAr) {
			// 		scope.selectedGccAr = true;
			// 		scope.dataobject.store_id = scope.list.checklist;
			// 		scope.list.checklist.push("6", "8", "10", "12");
			// 	} else {
			// 		scope.selectedGccAr = false;
			// 		scope.dataobject.store_id = [];
			// 	}
			// };

			// scope.checkUAEKSAEn = function() {
			// 	if (scope.selectedUAEKSAEn) {
			// 		scope.selectedUAEKSAEn = true;
			// 		scope.dataobject.store_id = scope.list.checklist;
			// 		scope.list.checklist.push("1","3");
			// 	} else {
			// 		scope.selectedUAEKSAEn = false;
			// 		scope.dataobject.store_id = [];
			// 	}
			// };
			// scope.checkUAEKSAAr = function() {
			// 	if (scope.selectedUAEKSAAr) {
			// 		scope.selectedUAEKSAAr = true;
			// 		scope.dataobject.store_id = scope.list.checklist;
			// 		scope.list.checklist.push("2","4");
			// 	} else {
			// 		scope.selectedUAEKSAAr = false;
			// 		scope.dataobject.store_id = [];
			// 	}
			// };
			////////////////// Commenting Multiselct checkbox group End
			
		}
		return directive;
	}
})();