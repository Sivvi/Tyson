(function(){
	'use strict';

	angular
		.module('spress')
		.directive('sourceandstores', [sourceandstores]);

	function sourceandstores(){
		var directive = new Object();
			directive.restrict = 'E';
			directive.scope = {
				 dataobject: '='
			};
			
			directive.link = link;
			directive.template = template;

		function template(element, attributes){
			
			

			return '<div class="sourcebox" ng-class="{active: visible}">'+
						'<span class="sourcebox_title sp_br4 pagesource" ng-click="showDrop()">Source & Store</span>'+
						'<ul class="sourcebox_list">'+
							'<li class="sourcebox_item">'+
								'<input type="checkbox" name="source" checklist-model="dataobject.source" value="ios" id="source_mw" class="sourcebox_check">'+
								'<label for="source_mw" class="sourcebox_label">iOS</label>'+
							'</li>'+
							'<li class="sourcebox_item">'+
								'<input type="checkbox" name="source" checklist-model="dataobject.source" value="android" id="source_ma" class="sourcebox_check">'+
								'<label for="source_ma" class="sourcebox_label">Android</label>'+
							'</li>'+
							'<li class="sourcebox_item">'+
								'<input type="checkbox" name="source" checklist-model="dataobject.source" value="mobile" id="source_dk" class="sourcebox_check">'+
								'<label for="source_dk" class="sourcebox_label">Mobile</label>'+
							'</li>'+
							'<li class="sourcebox_item">'+
								'<input type="checkbox" name="source" checklist-model="dataobject.source" value="desktop" id="source_dsk" class="sourcebox_check">'+
								'<label for="source_dsk" class="sourcebox_label">Desktop</label>'+
							'</li>'+
						'</ul>'+
						'<ul class="storebox_list">'+
						   '<li class="storebox_item" ng-repeat="store in stores">'+
						   '<input type="checkbox" name="stores" checklist-model="dataobject.store_id" checklist-value="store.value" id="{{store.id}}" class="storebox_check">'+
								'<label for="{{store.id}}" class="storebox_label">{{store.title}}</label>'+
							'</li>'+
						'</ul>'+
                    '</div>'; 
		}
		
        function link(scope, element, attributes){
			scope.stores = [
			    {
			        'id': 'store_ae_english',
			        'title': 'UAE English',
			        'value': 1,
			    },
			    {
			        'id': 'store_ae_arabic',
			        'title': 'UAE Arabic',
			        'value': 2,
			    },
			    {
			        'id': 'store_sa_english',
			        'title': 'KSA English',
			        'value': 3,
			    },
			    {
			        'id': 'store_sa_arabic',
			        'title': 'KSA Arabic',
			        'value': 4,
			    },
			    {
			        'id': 'store_om_english',
			        'title': 'Oman English',
			        'value': 5,
			    },
			    {
			        'id': 'store_om_arabic',
			        'title': 'Oman Arabic',
			        'value': 6,
			    },
			    {
			        'id': 'store_bh_english',
			        'title': 'Bahrain English',
			        'value': 7,
			    },
			    {
			        'id': 'store_bh_arabic',
			        'title': 'Bahrain Arabic',
			        'value': '8',
			    },
			    {
			        'id': 'store_kw_english',
			        'title': 'Kuwait English',
			        'value': 9,
			    },
			    {
			        'id': 'store_kw_arabic',
			        'title': 'Kuwait Arabic',
			        'value': 10,
			    },
			    {
			        'id': 'store_qa_english',
			        'title': 'Qatar English',
			        'value': 11,
			    },
			    {
			        'id': 'store_qa_arabic',
			        'title': 'Qatar Arabic',
			        'value': 12,
			    },
			    {
			        'id': 'store_int_english',
			        'title': 'Intl English',
					'value': 13,
			    },
			    {
			        'id': 'store_int_arabic',
			        'title': 'Intl Arabic',
			        'value': 14,
			    },
			];

			scope.visible = false;

			scope.showDrop = function(){
				scope.visible = !scope.visible;
				//console.log(scope.visible);
			}
		}
		return directive;
	}
})()