(function () {
    'use strict'

    angular
        .module('spress')
        .directive('addlandingdata', ['$timeout', addlandingdata]);

    function addlandingdata($timeout) {
        var directive = new Object();
        directive.restrict = "E";
        directive.scope = {
            dataobject: '='
        };
        directive.link = link;
        directive.templateUrl = templateUrl;



        function templateUrl(element, attributes) {
            return attributes.templateUrl;
        }

        function link(scope, element, attributes, $timeout) {
            scope.pop_landing = false;
			scope.showpoplandingdata = function(){
                
				scope.pop_landing = true;
			}
        }

        return directive;
    }

})()