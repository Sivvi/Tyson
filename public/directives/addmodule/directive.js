(function(){
	'use strict';

	angular
		.module('spress')
		.directive('addmodule', [addmodule]);

	function addmodule(){
		var directive = new Object();
			directive.restrict = 'E';
			directive.scope = {
				dataobject: '='
			};
			directive.templateUrl = templateUrl;
			directive.link = link;

		function templateUrl(element, attributes){
			return attributes.templateUrl;
		}

		function link(scope, element, attributes){
			scope.addModule = function(){
				//var id = scope.dataobject.length;
				scope.dataobject.push(new module());
			}

			function module(){
				this.Columns = new Array();
				this.AspectRatio = '';
			}
		}
		return directive;
	}
})()