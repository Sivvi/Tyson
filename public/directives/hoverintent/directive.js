(function(){
	'use strict';

	angular
		.module('spress')
		.directive('clickintent', ['$compile', '$parse', clickintent]);

	function clickintent($compile, $parse){
		var directive = {}; 
			directive.restrict= 'A';
			directive.link = link;

		function link(scope, element, attributes){
			var elm_subject = document.querySelector('.'+attributes.clickintent);
			var elm_object = element[0];
			var hover_time;

			//show dropdown on click
			elm_subject.addEventListener('click',function(event){
				elm_object.classList.add('active');
			});

			//hide dropdown if loose focus
			document.addEventListener('click', function(event){
				if(!isDescendant(elm_object, event.target) && !isDescendant(elm_subject, event.target)){
					elm_object.classList.remove('active');
				}
			});
		}

		function isDescendant(parent, child) {
		     var node = child.parentNode;
		     while (node != null) {
		         if (node == parent) {
		             return true;
		         }
		         node = node.parentNode;
		     }
		     return false;
		}
		return directive;
	};

})()