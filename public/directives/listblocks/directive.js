(function(){
	'use strict';

	angular
		.module('spress')
		.directive('listblocks', ['$timeout', 'constants', 'ngClickCopy', listblocks]);

	function listblocks($timeout, constants, ngClickCopy){
		var directive = new Object();
			directive.restrict = 'E';
			directive.scope = {
				list: '=',
				view: '&',
				parent: '<',
			};
			directive.templateUrl = templateUrl;
			directive.link = link;

		function templateUrl(element, attributes){
			return constants.directives+'listblocks/view.html';
		}

		function link(scope, element, attributes){
			scope.items = new Array();

			scope.clipboard = new Object();

			scope.$watch('list', function(newValue, oldValue){
				if(typeof newValue !== 'undefined'){
					scope.items = newValue;
				}
			},true);
			scope.sortLeft = function(index){
				var from = index, to = index-1;
				if(to >= 0){ 
					console.log();
					scope.list.splice(to, 0, scope.list.splice(from, 1)[0]);
				}
			}

			scope.sortRight = function(index){
				var from = index, to = index+1, max = scope.list.length;
				if(to < max){ 
					console.log('moved');
					scope.list.splice(to, 0, scope.list.splice(from, 1)[0]);
				}
				
			}

			scope.open = function(column_index){
				//console.log(scope.parent, column_index, scope.list);
				var column = findColumn(column_index);
				scope.view({'column': column, 'column_index': column_index});
			}

			scope.delete = function(block){
				scope.list = deleteBlock(block.ID, scope.list);
			}

			scope.blockWidth = function(){
				var layout = Math.floor(24/scope.list.length);
				return 'sp_small_'+layout+' sp_columns'
			}

			function findColumn(column_index){
				var result = null;
				angular.forEach(scope.list, function(column, index){
					if(index == column_index) result = column;
				});
				return result;
			}
			
			
			scope.copyAction = function(index){
                console.log(scope.items[index]);
				ngClickCopy.copy(scope.items[index]);
				//console.log('copy')
				//console.log(scope.items[index]);
				alert('Copied!');
			}
			scope.pasteAction = function(index){
				//console.log(scope.items[index]);
				var PasteBlock = window.confirm('Are you sure you want to paste here?');
				if (PasteBlock) {
                   // var pasteObj = Object.assign({}, ngClickCopy.paste()); 
					scope.items[index] = ngClickCopy.paste();
				}
			}
			
			
			function deleteBlock(index, list){
			var DelateData = window.confirm('Are you absolutely sure you want to delete this block?');
			if (DelateData) {
			  window.alert('Going to delete');
			  var count = 0, result = new Array();
				angular.forEach(list, function(value, ind){
                    if(index != value.ID) {
						result.push(value);
						count++;
					}
				});
				return result;
			}
				
			}
		}
		
		return directive;
	}
})()