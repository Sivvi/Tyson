(function(){
	'use strict';

	angular
		.module('spress')
		.directive('column', [column]);

	function column(){
		var directive = new Object();
			directive.restrict = 'E';
			directive.transclude = true;
			directive.scope = true;
			directive.link = link;
			directive.templateUrl = templateUrl;

		function templateUrl(element, attributes){
			return attributes.templateUrl;
		}

		function link(scope, element, attributes){
			scope.data = new Object();
			scope.templates = [
				{'id': 0,'label': 'SubCategory'},
				{'id': 1,'label': 'Products'},
				{'id': 2,'label': 'Sports'},
				{'id': 3,'label': 'Brands'},
				{'id': 4,'label': 'Campaign'},
				{'id': 5,'label': 'GiftGuide'},
				{'id': 6,'label': 'Celebrities'},
				{'id': 7,'label': 'HTML'},
				{'id': 8,'label': 'Landing'},
				{'id': 9,'label': 'SportsList'},
			];


			scope.save = function(){
				scope.data['LandingData'] = new Array();
				scope.dataobject.Columns.push(scope.data);

				$('body').removeClass('noscroll');
				scope.pop = false;
			}

			function reset(){
				scope.data = new Object();
			}
		}

		return directive;
	}
})()