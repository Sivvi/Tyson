(function () {
    'use strict'

    angular
        .module('spress')
        .directive('listimages', ['$timeout', listimages]);

    function listimages($timeout) {
        var directive = new Object();
        directive.restrict = "E";
        directive.scope = {
            imagelist: '='
        };
        directive.link = link;
        directive.templateUrl = templateUrl;

        function templateUrl(element, attributes) {
            return attributes.templateUrl;
        }

        function link(scope, element, attributes, $timeout) {
            // scope.showpopeditimage = function(){
			// 	$('body').addClass('noscroll');
			// 	scope.data = scope.dataobject;
			// 	scope.popeditimage = true;
				
			// }
            scope.delete = function (index) {
                scope.imagelist.splice(scope.imagelist.indexOf(scope.imagelist), 1);
            }
        }

        return directive;
    }

})()