/*
* Version : 2
* Date : 08-June-2017
* NodeJS Server for Solr + Magento API Requests
*/

var common = require("../services/common.js");
var request = require('request');
var express = require('express');
var extend = require('extend');
var config = require("../config");
var winston = require('winston');
var http = require("http"); // http
var https = require("https"); //https
var app = module.exports = express();
/*
 * Use
 */
app.use(function (req, res, next) {
    //res.header("Access-Control-Allow-Origin", "*");
    //res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    res.header('Access-Control-Request-Headers', 'Authorization, Access-Control-Allow-Origin');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Authorization, Content-type, Access-Control-Allow-Origin');
    res.header("Access-Control-Allow-Origin", "*");
    next();
});
app.get('/', function (req, res) {
    res.sendStatus(404);
});

/*=====================================================================
    Prototype on String for validate json
=======================================================================*/
String.prototype.jsonValidate = String.prototype.jsonValidate || function () {
    return (/^[\],:{}\s]*$/.test(this.replace(/\\["\\\/bfnrtu]/g, '@').replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').replace(/(?:^|:|,)(?:\s*\[)+/g, '')));
}

/*******  General Functions   *******/
performRequest = function (endpoint, method, data, success) {

    var dataString = JSON.stringify(data);
    var headers = {};

    if (method == 'GET') {
        endpoint += '?' + querystring.stringify(data);
    }
    else {

        headers = {
            'Content-Type': 'application/json; charset=utf-8',
            //'Content-Length': dataString.length,
            'Content-Length': Buffer.byteLength(dataString, 'utf8'),
            "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36"
        };

    }

    var options = {
        "hostname": config.magento.host,
        "port": null,
        "path": config.magento.path,
        method: method,
        headers: headers
    };
    var req = http.request(options, function (res) {

        res.setEncoding('utf-8');


        var responseString = '';

        res.on('data', function (data) {
            responseString += data;
        });

        res.on('end', function () {

            if (!(responseString.jsonValidate())) {

                // Log error
                var logger = new (winston.Logger)({
                    transports: [
                        new (winston.transports.Console)(),
                        new (winston.transports.File)({filename: './error.log'})
                    ]
                });

                logger.error(responseString);

                var err = new Error("Error occured");
                var results = {status: "error"};
                success(results);
            }
            else {
                var responseObject = JSON.parse(responseString);
                success(responseObject);
            }
        });
    });

    // body = new Buffer(dataString, 'binary');
    // conv = new Iconv('latin1', 'utf8');
    // body = conv.convert(body).toString();


    req.write((dataString));
    req.end();
};
/*******  General Functions End   *******/


/*******  Catalog Related API Calls   *******/

/*
* Generate Token
*/
app.post('/sPAuthenticate', function (req, res) {

    var checkCredentials = common.checkApiCredentials(req.body);
    if (checkCredentials === false) {
        return common.sendResponse(200, 'we caught you', 'error', res);
    }

    // pass options into token generation
    var opts = {username: common.username, password: common.password};
    var sessionToken = common.generateAndStoreToken(req, opts);
    var results = {sessionToken: sessionToken};
    return common.sendResponse(200, 'success', results, res);
    //res.send(JSON.stringify(results));
    res.end();
});

/*
* Product Listing
*/
app.post('/pList', function (req, res) {

    var checkParams = common.checkApiParams(req.body);
    if (checkParams.status === false) {
        return common.sendResponse(200, checkParams.message, 'error', res);
    }

    var storeid = req.body.store_id;
    var params = req.body.params;
    var query = req.body.query + ' AND store_id: ' + storeid;
    var sort = params.sort;

    if (sort === 'discover') {
        rrDiscover(req, res);
    }
    else {
        //solr call
        var send = common.solrRequest(query, params);
        request(send, function (error, response, body) {
            var result = JSON.parse(body);
            res.send(result);
        });
    }
});


/*******  Catalog Related API Calls End   *******/


/*******  Magento Related API Calls   *******/

/*
 * CORS middleware
 */
// var allowCrossDomain = function(req, res, next) {
//     res.header('Access-Control-Allow-Origin', '*');
//     res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
//     res.header('Access-Control-Allow-Headers', 'Content-Type');
// }


/*
* authservice
* Required customer to be loggedin
*/
app.post('/sAuth', function (req, res) {

    var checkParams = common.checkApiParams(req.body);
    if (checkParams.status === false) {
        return common.sendResponse(200, checkParams.message, 'error', res);
    }

    // cross domain
    //app.use(allowCrossDomain);

    // params
    var token = req.body.token;
    var params = req.body;

    // validate token
    var decoded = common.verify(token);

    var response = res;
    var request = req;

    if (!decoded || !decoded.auth) {

        var results = {status: "false"};
        res.send(JSON.stringify(results));
        res.end();

    } else {

        delete params["token"];
        params["customer"] = true;
        params["username"] = decoded.username;
        params["password"] = decoded.password;

        jsonParams = JSON.stringify(params);

        // perform request if token is valid
        performRequest('bw', 'POST', params, function (data) {

            if (data.status == "error") {

                var results = {status: "error"};
                res.send(JSON.stringify(data));
                res.end();

            }
            else {

                var results = {status: "success"};
                res.send(JSON.stringify(data));
                res.end();

            }

        });

    }
});

/*
 * Login
 */
app.post('/sPLogin', function (req, res) {

    var checkParams = common.checkApiParams(req.body);
    if (checkParams.status === false) {
        return common.sendResponse(200, checkParams.message, 'error', res);
    }
    var user = req.body.username;
    var pswd = req.body.password;

    var response = res;
    var request = req;

    performRequest('bw', 'POST', {

        action: "customer",
        username: user,
        password: pswd

    }, function (data) {

        if (data.status == "error") {

            var results = {status: "error"};
            res.send(JSON.stringify(results));
            res.end();

        }
        else {

            // pass options into token generation
            var opts = {username: user, password: pswd}
            var token = common.generateAndStoreToken(req, opts);
            var results = {status: "success", token: token, customer: data};
            res.send(JSON.stringify(results));
            res.end();

        }
    });
});

/*
 * Validate
 */
app.post('/sValidate', function (req, res) {

    var checkParams = common.checkApiParams(req.body);
    if (checkParams.status === false) {
        return common.sendResponse(200, checkParams.message, 'error', res);
    }

    //app.use(allowCrossDomain);
    var token = req.body.token;
    var decoded = common.verify(token);

    if (!decoded || !decoded.auth) {

        var results = {status: "error"};
        res.send(JSON.stringify(results));
        res.end();

    } else {

        // perform request
        performRequest('bw', 'POST', {

            action: "customer",
            username: decoded.username,
            password: decoded.password

        }, function (data) {

            var results = {status: "success", customer: data};
            res.send(JSON.stringify(results));
            res.end();

        });
    }

});
/*******  Magento Related API Calls End  *******/


/*******  S-Press Related API Calls  *******/

/*
* Get App Config File
*/
app.post('/sPGetAppConfig', function (req, res) {

    var checkParams = common.checkApiParams(req.body);
    if (checkParams.status === false) {
        return common.sendResponse(200, checkParams.message, 'error', res);
    }

    var params = req.body.params;

    //solr call
    var send = common.solrSpressRequest(req, params);
    request(send, function (error, response, body) {
        if (common.isJsonString(body)) {
            var result = JSON.parse(body);
            var parsed_data = JSON.parse(result.response.docs[0].data);
            result.response.docs[0].data = parsed_data;

            res.send(result);
        }
        else {
            res.send(response);
        }
    });
});

/*
* Save Config File | Old - Not in use 
*/
app.post('/sPSaveAppConfig', function (req, res) {
    var checkParams = common.checkApiParams(req.body);
    if (checkParams.status === false) {
        return common.sendResponse(200, checkParams.message, 'error', res);
    }

    var data_details = JSON.parse(req.body.data_details);
    var data = JSON.parse(req.body.data);

    var source = data_details.source;
    var stores = data_details.store_id;

    var success = Array();
    var error = Array();
    source.forEach(function (sourceid) {
        stores.forEach(function (storeId) {
            data_details.source = sourceid;
            data_details.store_id = storeId;

            common.solrSpressUpdateRequest(req, JSON.stringify(JSON.stringify(data_details)), JSON.stringify(JSON.stringify(data)), function (response) {
                if (response.error) common.sendResponse(400, response.error, 'error', res);
                else success.push(response.result); // common.sendResponse(200,response.result,'success',res);
            });
        });
    });
    common.sendResponse(200, {}, 'success', res);
});

/*
* Save documents multiple
*/
app.post('/sPSaveAppConfigBulk', function (req, res) {
    
    var checkParams = common.checkApiParams(req.body);
    if (checkParams.status === false) {
        return common.sendResponse(200, checkParams.message, 'error', res);
    }

    var dataDetails = JSON.parse(req.body.data_details);
    var data = JSON.parse(req.body.data);

    var source = dataDetails.source;
    var stores = dataDetails.store_id;
    var pushData = [];
    


    source.forEach(function (sourceId) {
        stores.forEach(function (storeId) {
            var obj = {};
            for (var key in dataDetails) {
                if (!dataDetails.hasOwnProperty(key)) {
                    continue;
                }
                obj[key] = dataDetails[key];
            }
            obj.source = sourceId;
            obj.store_id = storeId;
            obj.data = JSON.stringify(JSON.stringify(data));

            pushData.push(obj);
        });
    });
    common.solrSpressBulkInsert(req, pushData, function (response) {
        if (response.error) common.sendResponse(400, response.error, 'error', res);
        else common.sendResponse(200, response.result, 'success', res);
    });
});

/*
* Save Config File
*/
app.post('/sPDeleteAppConfig', function (req, res) {

    var checkParams = common.checkApiParams(req.body);
    if (checkParams.status === false) {
        return common.sendResponse(200, checkParams.message, 'error', res);
    }

    //solr call
    var response = common.solrSpressDeleteRequest(req, req.body.data, function (response) {
        if (response.error) common.sendResponse(400, response.error, 'error', res);
        else common.sendResponse(200, response.result, 'success', res);
    });
});


/*
* Save Category Tree
*/
app.post('/sPSaveCategoryTree', function (req, res) {

    var checkParams = common.checkApiParams(req.body);
    if (checkParams.status === false) {
        return common.sendResponse(200, checkParams.message, 'error', res);
    }

    //solr call
    var response = common.solrCategoryTreeUpdateRequest(req, req.body.data_details, req.body.data, function (response) {
        if (response.error) common.sendResponse(400, response.error, 'error', res);
        else common.sendResponse(200, response.result, 'success', res);
    });

});

/*
* Category Tree Indexer
*/
app.get('/sPCTreeIndexer', function (req, response, next) {

    function getExistingTreeVersion(req) {

        return new Promise(
            function (resolve, reject) {
                // Get Existing Version
                var params = {};
                req.body.query = 'status:publish';
                params.rows = '1';
                params.wt = 'json';
                params.fl = 'version';
                var old_version = '0';

                var send = common.solrCategoryTreeRequest(req, params);

                request(send, function (error, response, body) {
                    if (error) reject(error);
                    else {
                        if (response) {
                            if (common.isJsonString(body)) {
                                var result = JSON.parse(body);
                                if (result.response.numFound > 0) {
                                    old_version = result.response.docs[0].version;
                                }
                                resolve(old_version);
                            }
                        }
                    }
                });
            }
        );
    }

    function makeVersion(date) {
        var dd = date.getDate();
        var mm = date.getMonth() + 1; //January is 0!
        var yy = date.getFullYear();
        var newVersion = dd + "-" + mm + "-" + yy;
        return newVersion;
    }

    function getNewCategoryTree(req) {
        return new Promise(
            function (resolve, reject) {
                var params = req;
                var categoryData;
                performRequest('bw', 'POST', params, function (data) {
                    if (data) {
                        categoryData = JSON.stringify(data);
                    }
                    resolve(categoryData);
                });
            }
        );
    }

    function addNewCategoryInSolr(req, data_details, tree) {
        return new Promise(
            function (resolve, reject) {
                //Adding New Tree on Solr
                common.solrCategoryTreeUpdateRequest(req, data_details, tree, function (response) {
                    resolve(response);
                });
            }
        );
    }

    function addNewCategoryTree(req, stores, tree) {

        return new Promise(
            function (resolve, reject) {
                var today = new Date();
                var new_version = makeVersion(today);

                var data_details;
                var res;
                stores.forEach(function (storeId) {
                    console.log(storeId);
                    data_details = '{"version":"Version-' + new_version + '","source":"ios","store_id":"' + storeId + '","status":"publish"}';

                    addNewCategoryInSolr(req, data_details, tree)
                        .then(function (solrcategoryResponse) {
                            if (solrcategoryResponse) {
                                if (solrcategoryResponse.error === null) {
                                    res = 'success';
                                }
                            }
                            resolve(res);
                        });
                });
            }
        );
    }

    function deleteExistingTreeInSolr(req, version) {
        return new Promise(
            function (resolve, reject) {
                var date = new Date();
                var yesterday = date - 1000 * 60 * 60 * 24 * 1;   // current date's milliseconds - 1,000 ms * 60 s * 60 mins * 24 hrs * (# of days beyond one to go back)
                yesterday = new Date(yesterday);
                var oldVersion = makeVersion(yesterday);
                version = "Version-" + oldVersion;
                console.log('yesterday');
                console.log(version);
                //Delete Trees on Solr
                var dataDetails = {'version': version};
                common.solrCategoryTreeDeleteRequest(req, dataDetails, function (response) {
                    resolve(response);
                });
            }
        );
    }

    function addTree(req) {

        return new Promise(
            function (resolve, reject) {

                var storesCodes = ['en', 'ar'];
                var en_stores = ['1', '3', '5', '7', '9', '11', '13'];
                var ar_stores = ['2', '4', '6', '8', '10', '12', '14'];
                //var en_stores = ['1','3'];
                //var ar_stores = ['2','4'];
                var stores;
                var store;
                storesCodes.forEach(function (storeCode) {
                    if (storeCode == 'en') {
                        store = 1;
                    }
                    else {
                        store = 2;
                    }

                    // Get New Category Tree from Magento
                    var categoryReq = {'action': 'solr_categories', 'store_id': store};

                    getNewCategoryTree(categoryReq)
                        .then(function (tree) {
                            if (tree) {
                                if (storeCode == 'en') {
                                    stores = en_stores;
                                }
                                else {
                                    stores = ar_stores;
                                }
                                // Add New Category Tree in Solr
                                addNewCategoryTree(req, stores, tree)
                                    .then(function (newcategoryResponse) {
                                        if (newcategoryResponse) {
                                            console.log(newcategoryResponse);

                                            resolve(newcategoryResponse);
                                        }
                                    });
                            }
                        });
                });
            }
        );
    }

    // Get existing category tree version
    addTree(req)
        .then(function (res) {
            console.log(res);
            // After Successul addition of category tree in Solr
            if (res === "success") {
                // Delete old category tree version
                var version;
                deleteExistingTreeInSolr(req, version)
                    .then(function (data) {
                        if (data) {
                            console.log(data);
                        }
                    });
                common.sendResponse(200, response.result, 'success', response);
            }
        });
});

/*******  S-Press Related API Calls End  *******/