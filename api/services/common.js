var base64url = require("base64url");
var config = require("../config");
var common = new Object();
var jwt = require('jsonwebtoken'); // web tokens
var SolrNode = require('solr-node');
//var solr = require('../public/solr/main');
var solr = require('solr-client');

// Route js Common Functions
var secret = "unlike_autonomy_observation_response"; // super secret
//var solrClient = require('solr-client');
/*
 * Verify
 * @param {type} token
 * @returns {Boolean|Object.verify.decoded}
 */
common.verify = function (token) {
    var decoded = false;
    try {
        decoded = jwt.verify(token, secret);
    } catch (e) {
        decoded = false; // still false
    }
    return decoded;
};

/*
 * generateGUID
 * @returns {Number}
 */
common.generateGUID = function () {
    return new Date().getTime(); // we can do better with crypto
};

/*
 * Generate New Token & Store
 * @param {type} req
 * @param {type} opts
 * @returns {token}
 */
common.generateAndStoreToken = function (req, opts) {
    var GUID = common.generateGUID(); // write/use a better GUID generator in practice
    var token = common.generateToken(req, GUID, opts);
    var record = {
        "valid": true,
        "created": new Date().getTime()
    };
    return token;
};

/*
 * Generate Token
 * @param {type} req
 * @param {type} GUID
 * @param {type} opts
 * @returns {token}
 */
common.generateToken = function (req, GUID, opts) {

    opts = opts || {};

    // By default, expire the token after 30 days.
    var days = 30;
    var hour = 24;
    var min = 60;
    var sec = 60;
    //var expiresDefault = Math.floor(new Date().getTime()/1000) + days*hour*min*sec;
    var expiresDefault = Math.floor(new Date().getTime() / 1000) + days * hour * min * sec;

    var token = jwt.sign({
        username: opts.username,
        password: opts.password,
        auth: GUID,
        agent: req.headers['user-agent'],
        exp: opts.expires || expiresDefault
    }, secret);

    return token;
};
// Route js Common Functions End


common.username = 'sivvi';
common.password = 'N0*6%XEwb:!2Y|m';

/**
 * Convert store codes to area codes
 * @param  {number} storeid store id
 * @return {String}         area code
 */
common.regionCodes = function (storeid) {
    var data = {
        '1': 'en-ae',
        '2': 'ar-ae',
        '3': 'en-sa',
        '4': 'ar-sa',
        '5': 'en-om',
        '6': 'ar-om',
        '7': 'en-bh',
        '8': 'ar-bh',
        '9': 'en-kw',
        '10': 'ar-kw',
        '11': 'en-qa',
        '12': 'ar-qa',
        '13': 'en-us',
        '14': 'ar-us',
    }
    return data[storeid];
}


/**
 * Run regex and return all occurrances
 * @param  {Object} exp   A regular expression Object
 * @param  {String} value string on which to run regex
 * @return {Array}       Array of all occurrances
 */
common.regexAll = function (exp, value) {
    var match, result = new Array();
    while ((match = exp.exec(value)) != null) result.push(match[1]);
    return result;
}


/**
 * Parse categoryId from query
 * @param  {String} query Query string from client
 * @return {String}       Category id
 */
function parseCategory(query) {
    var result = new Object();
    var category_rrex = new RegExp(/(?:category_ids:)(\d+)/, 'ig');
    result = category_rrex.exec(query)[1];
    return result;
}


/**
 * Parse brands from query
 * @param  {String} query Query string from client
 * @return {String}       pipe seprated string of all found brands
 */
function parseBrands(query) {
    var result = '';
    var brands_rrex = new RegExp(/(?:select_designer)(?:\:\s)(?:\")([a-zA-Z0-9_.&'+é ]*)(?:\")/, 'ig');
    var arr = common.regexAll(brands_rrex, query);
    if (arr.length > 0) result = arr.join('|');
    return result;
}


/**
 * Parse filters from query
 * @param  {String} query Query sent from client
 * @return {String}       pipe seperated string of all filters
 */
function parseFilters(query) {
    var result = Array();
    var rexes = {
        'color': new RegExp(/(?:color)(?:\:\s)(?:\")(\w+)(?:\")/, 'ig'),
        'clothing_size': new RegExp(/(?:size_filters)(?:\:\s)(?:\")(\w+)(?:\")/, 'ig'),
        'bra_size': new RegExp(/(?:bra_size)(?:\:\s)(?:\")(\w+)(?:\")/, 'ig'),
        'ring_size': new RegExp(/(?:ring_size)(?:\:\s)(?:\")(\w+)(?:\")/, 'ig'),
        'belt_size': new RegExp(/(?:belt_size)(?:\:\s)(?:\")(\w+)(?:\")/, 'ig'),
        'hat_size': new RegExp(/(?:hat_size)(?:\:\s)(?:\")(\w+)(?:\")/, 'ig'),
        'shoe_size': new RegExp(/(?:shoe_size)(?:\:\s)(?:\")(\w+)(?:\")/, 'ig'),
        'sports_acc_size': new RegExp(/(?:sports_acc_size)(?:\:\s)(?:\")(\w+)(?:\")/, 'ig'),
        'sports_eqp_size': new RegExp(/(?:sports_eqp_size)(?:\:\s)(?:\")(\w+)(?:\")/, 'ig'),
        'makeup_color': new RegExp(/(?:makeup_color)(?:\:\s)(?:\")(\w+)(?:\")/, 'ig'),
    };

    for (var key in rexes) {
        if (rexes.hasOwnProperty(key)) {
            var arr = common.regexAll(rexes[key], query);
            if (arr.length > 0) {
                var val = '\"' + arr.join('\";\"') + '\"';
                result.push(key + ':' + val);
            }
        }
    }
    return result.join('|');
}


/**
 * [languageCodes description]
 * @param  {[type]} storeid [description]
 * @return {[type]}         [description]
 */
common.languageCodes = function (storeid) {
    var eng = ['1', '3', '5', '7', '9', '11', '13'];
    var ar = ['2', '4', '6', '8', '10', '12', '14'];

    if (eng.indexOf(storeid) > -1) return 'en-ae';
    if (ar.indexOf(storeid) > -1) return 'ar-ae';
}


/**
 * Solr request options
 * @param  {String} query  Initial solr query
 * @param  {Object} params client request parameters
 * @return {Object}        request options
 */
common.solrRequest = function (query, params) {
    var queryrex = new RegExp(/(?:\()(?:\d+\s+OR\s+(?:\d+){0,1})+(?:\))/, 'ig');
    var send = {
        url: config.solr.host + config.solr.path,
        method: 'POST',
        headers: {
            'content-type': config.solr.contenttype,
            'cache-control': "no-cache",
        },
        qs: {
            'q': query
        },
        useQuerystring: true
    };

    if (queryrex.test(query)) {
        send.qs.rows = 200;
        send.qs.wt = (params.wt) ? params.wt : 'json';
        send.qs.fl = (params.fl) ? params.fl : '';
    }
    else {
        for (var key in params) {
            if (params.hasOwnProperty(key)) send.qs[key] = params[key];
        }
        if (send.qs.sort == 'discover') send.qs.sort = params.fsort;
    }
    return send;
}

/**
 * solrSpress request options
 * @param  {String} query  Initial solr query
 * @param  {Object} params client request parameters
 * @return {Object}        request options
 */
common.solrSpressRequest = function (req, params) {
    var host = req.headers.host;
    var live_host = config.host.url;
    var ssconfig = (host === live_host) ? config.spressSolr.live : config.spressSolr.dev;
    var query = req.body.query;
    var send = {
        //url: 'http://'+ssconfig.host+':'+ssconfig.port+ssconfig.path,
          url: ssconfig.protocol + '://' + ssconfig.host + ':' + ssconfig.port + ssconfig.path,
        method: 'POST',
        headers: {
            'content-type': ssconfig.contenttype,
            'cache-control': "no-cache",
        },
        qs: {
            'q': query
        },
        useQuerystring: true
    };

    send.qs.rows = (params.rows) ? params.rows : '100';
    send.qs.wt = (params.wt) ? params.wt : 'json';
    send.qs.fl = (params.fl) ? params.fl : '';
    send.qs.sort = params.fsort;

    return send;
};

/**
 * solrSpress Update request options
 * @param  {String} query  Initial solr query
 * @param  {Object} params client request parameters
 * @return {Object}        request options
 */
common.solrSpressUpdateRequest = function (req, data_details, data, callback) {
    var host = req.headers.host;
    var live_host = config.host.url;
    var ssconfig = (host === live_host) ? config.spressSolr.live : config.spressSolr.dev;
    var res = {'error': false, 'result': new Object()};
    var status = 'success';
    // Create client
    var client = new SolrNode({
        host: ssconfig.host, //'127.0.0.1',
        port: ssconfig.port, //'8983',
        core: ssconfig.core,//'spress',
        protocol: ssconfig.protocol,
        debugLevel: false
    });

    var data_details = JSON.parse(data_details);
    // append landing data in each module to end of the json as independent modules with module title as the first item title
    // data = prepareLandingDataBeforePush(data); // disabled
    var data_content = JSON.stringify(data);
    data_details.data = data_content;

    // Update document to Solr server
    client.update(data_details, function (err, result) {
        if (!common.isJsonString(result)) {
            res['error'] = err;
        }
        else {
            res['result'] = result;
        }
        callback(res);
    });

};

/**
 * Save Multiple documents
 * @param req
 * @param data
 * @param callback
 */
common.solrSpressBulkInsertRequest = function (req, data, callback) {
    var host = req.headers.host;
    var live_host = config.host.url;
    var ssConfig = (host === live_host) ? config.spressSolr.live : config.spressSolr.dev;

    var res = '';
    // Create client
    var client = new SolrNode({
        host: ssConfig.host, //'127.0.0.1',
        port: ssConfig.port, //'8983',
        core: ssConfig.core,//'spress',
        protocol: ssConfig.protocol,
        debugLevel: false
    });
    
    client.update(data, function (err, response) {
        if (err) {
            res['error'] = err;
        } else {
            res['result'] = response;
        }
        callback(res);
    });
};


/**
 * Save Multiple documents | In use 
 * @param req
 * @param data
 * @param callback
 */
common.solrSpressBulkInsert = function (req, data, callback) {
    var host = req.headers.host;
    var live_host = config.host.url;
    var ssConfig = (host === live_host) ? config.spressSolr.live : config.spressSolr.dev;

    var res = '';
    // Create client

    var client = new solr.createClient({
        host: ssConfig.host, //'127.0.0.1',
        port: ssConfig.port, //'8983',
        core: ssConfig.core,//'spress',
        protocol: ssConfig.protocol,
        debugLevel: false
    });
    
    //client.autoCommit = true;
    
    client.add(data, function (err, response) {
        if (err) {
            res['error'] = err;
            
            client.rollback(function(err,obj){
                if(err){
                     console.log(err);
                }else{
                     console.log(obj);	
                }
            });
            
        } else {
            res['result'] = response;
            client.commit(function(err,res){
                if(err) console.log(err);
                if(res) console.log(res);
             });

        }
//        console.log(res);
        callback(res);
    });
};

/**
 * Save Multiple documents
 * @param req
 * @param data
 * @param callback
 */
common.solrSpressBulkInsertRequest = function (req, data, callback) {
    var host = req.headers.host;
    var live_host = config.host.url;
    var ssConfig = (host === live_host) ? config.spressSolr.live : config.spressSolr.dev;

    var res = '';
    // Create client
    var client = new SolrNode({
        host: ssConfig.host, //'127.0.0.1',
        port: ssConfig.port, //'8983',
        core: ssConfig.core,//'spress',
        protocol: ssConfig.protocol,
        debugLevel: false
    });
    
console.log('************');
console.log(data);
    client.add(data, function (err, response) {
        if (err) {
            res['error'] = err;
        } else {
            res['result'] = response;
        }
        callback(res);
    });
};

/**
 * solrSpress Update request options
 * @param  {String} query  Initial solr query
 * @param  {Object} params client request parameters
 * @return {Object}        request options
 */
common.solrSpressDeleteRequest = function (req, data, callback) {
    var host = req.headers.host;
    var live_host = config.host.url;
    var ssconfig = (host === live_host) ? config.spressSolr.live : config.spressSolr.dev;
    var res = {'error': false, 'result': new Object()};
    var status = 'success';
    // Create client
    var client = new SolrNode({
        host: ssconfig.host, //'127.0.0.1',
        port: ssconfig.port, //'8983',
        core: ssconfig.core,//'spress',
        protocol: ssconfig.protocol,
        debugLevel: false
    });

    // JSON Data
    //var data = JSON.parse(req.body.data);
    /*var data = 'unique:adf656d5-7c04-44ae-a822-9d645b1a505e'*/

    // Update document to Solr server
    client.delete(data, function (err, result) {
        if (!common.isJsonString(result)) {
            res['error'] = err;
        }
        else {
            res['result'] = result;
        }
        callback(res);
    });
};

/**
 * solrCategoryTree Update request options
 * @param  {String} query  Initial solr query
 * @param  {Object} params client request parameters
 * @return {Object}        request options
 */
common.solrCategoryTreeUpdateRequest = function (req, data_details, data, callback) {
    var host = req.headers.host;
    var live_host = config.host.url;
    var ssconfig = (host === live_host) ? config.scategoryTressSolr.live : config.scategoryTressSolr.dev;
    var res = {'error': false, 'result': new Object()};
    var status = 'success';
    // Create client
    var client = new SolrNode({
        host: ssconfig.host, //'127.0.0.1',
        port: ssconfig.port, //'8983',
        core: ssconfig.core,//'spress',
        protocol: ssconfig.protocol,
        debugLevel: false
    });

    var data_details = JSON.parse(data_details);
    var data_content = data;

    data_details.data = data_content;

    // Update document to Solr server
    client.update(data_details, function (err, result) {
        if (!common.isJsonString(result)) {
            res['error'] = err;
        }
        else {
            res['result'] = result;
        }
        callback(res);
    });
};

/**
 * solrCategoryTree Delete request options
 * @param  {String} query  Initial solr query
 * @param  {Object} params client request parameters
 * @return {Object}        request options
 */
common.solrCategoryTreeDeleteRequest = function (req, data, callback) {
    var host = req.headers.host;
    var live_host = config.host.url;
    var ssconfig = (host === live_host) ? config.scategoryTressSolr.live : config.scategoryTressSolr.dev;
    var res = {'error': false, 'result': new Object()};
    var status = 'success';
    // Create client
    var client = new SolrNode({
        host: ssconfig.host, //'127.0.0.1',
        port: ssconfig.port, //'8983',
        core: ssconfig.core,//'spress',
        protocol: ssconfig.protocol,
        debugLevel: false
    });

    // JSON Data
    //var data = JSON.parse(req.body.data);
    /*var data = 'unique:adf656d5-7c04-44ae-a822-9d645b1a505e'*/

    // Update document to Solr server
    client.delete(data, function (err, result) {
        if (!common.isJsonString(result)) {
            res['error'] = err;
        }
        else {
            res['result'] = result;
        }
        callback(res);
    });
};

/**
 * solrSpress request options
 * @param  {String} query  Initial solr query
 * @param  {Object} params client request parameters
 * @return {Object}        request options
 */
common.solrCategoryTreeRequest = function (req, params) {
    var host = req.headers.host;
    var live_host = config.host.url;
    var ssconfig = (host === live_host) ? config.scategoryTressSolr.live : config.scategoryTressSolr.dev;
    var query = req.body.query;
    var send = {
        //url: 'http://'+ssconfig.host+':'+ssconfig.port+ssconfig.path,
        url: ssconfig.protocol + '://' + ssconfig.host + ssconfig.path,
        method: 'POST',
        headers: {
            'content-type': ssconfig.contenttype,
            'cache-control': "no-cache",
        },
        qs: {
            'q': query
        },
        useQuerystring: true
    };

    send.qs.rows = (params.rows) ? params.rows : '100';
    send.qs.wt = (params.wt) ? params.wt : 'json';
    send.qs.fl = (params.fl) ? params.fl : '';
    send.qs.sort = params.fsort;

    return send;
};

/**
 * Extract product ids from products array
 * @param  {Array} array products array
 * @return {Array}       ids array
 */
common.extIds = function (array) {
    var results = new Array();
    for (var i = 0; i < array.length; i++) {
        var product = array[i];
        results.push(product.id);
    }
    return results;
};


/**
 * Extract product trancking links from products array
 * @param  {Array} array products array
 * @return {Array}       links array
 */
common.extTrackLinks = function (array) {
    var results = new Array();
    for (var i = 0; i < array.length; i++) {
        var product = array[i];
        results.push(product.clickTrackingURL);
    }
    return results;
}


/**
 * [findProductById description]
 * @param  {[type]} id    [description]
 * @param  {[type]} array [description]
 * @return {[type]}       [description]
 */
common.findProductById = function (id, array) {
    var result = null;
    for (var i = 0; i < array.length; i++) {
        var prod = array[i];
        if (id == prod.id) result = prod;
    }
    return result;
}


/**
 * [searchResult description]
 * @param  {[type]} solr  [description]
 * @param  {[type]} rich  [description]
 * @param  {[type]} found [description]
 * @param  {[type]} start [description]
 * @return {[type]}       [description]
 */
common.searchResult = function (solr, rich, found, start) {
    var result = new Object();
    if (typeof rich !== 'undefined') {
        result.numFound = found;
        result.start = start;
        result.docs = new Array()
        for (var i = 0; i < rich.length; i++) {
            var rr_prod = rich[i];
            var solr_prod = common.findProductById(rr_prod.id, solr.docs);

            if (solr_prod != null) {
                solr_prod['trackingUrl'] = rr_prod.clickTrackingURL;
                solr_prod['richid'] = rr_prod.id;
                result.docs.push(solr_prod);
            }
        }
    }
    else {
        for (var i = 0; i < solr.docs.length; i++) {
            solr.docs[i]['trackingUrl'] = null;
            solr.docs[i]['richid'] = null;
        }
        result = solr;
    }
    return result;
}


/**
 * Send formated response to client
 * @param  {Number} status        response status
 * @param  {String} statusMessage response message
 * @param  {Object} result        server response
 * @param  {Object} res           client response object
 */
common.sendResponse = function (status, statusMessage, result, res) {
    var stdResponse = {};
    stdResponse.status = status;
    stdResponse.result = result;
    stdResponse.statusMessage = statusMessage;

    // res.header('Access-Control-Request-Headers', 'Authorization, Access-Control-Allow-Origin');
    // res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
    // res.header('Access-Control-Allow-Headers', 'Authorization, Content-type, Access-Control-Allow-Origin');
    // res.header("Access-Control-Allow-Origin", "*");

    // res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    // res.header('Access-Control-Request-Headers', 'Authorization, Access-Control-Allow-Origin');
    // res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
    // res.header('Access-Control-Allow-Headers', 'Authorization, Content-type, Access-Control-Allow-Origin');
    // res.header("Access-Control-Allow-Origin", "*");

    res.send(stdResponse);
};


/**
 * Rich-relevance parameters validation
 * @param  {Object} params params object
 * @return {Boolean}        result of validation (true/false)
 */
common.checkParams = function (params) {
    if (Object.keys(params).length < 1 || !('key' in params) || !('userId' in params) || !('sessionId' in params) || !('page' in params) || !('store_id' in params)) {
        return false;
    }
    return true;
};

/**
 * API Parameters validation
 * @param  {Object} params params object
 * @return {Boolean}        result of validation (true/false)
 */

var requiredParams = ['sessionToken'];

common.checkApiParams = function (req) {
    var response = {};
    response.status = true;
    response.message = 'success';

    if (Object.keys(req).length < 1) {
        response.status = false;
        response.message = 'all required parameters missing';
    }
    else {
        requiredParams.forEach(function (param) {
            if (!(param in req)) {
                response.status = false;
                response.message = param + ' missing';
            }
        });
    }

    if (response.status === true) {
        var checkParams = common.tokenVerify(req.sessionToken);
        if (checkParams === false) {
            response.status = false;
            response.message = 'wrong token';
        }
    }

    return response;
};

/**
 * API Parameters validation
 * @param  {Object} params params object
 * @return {Boolean}        result of validation (true/false)
 */
common.generateApiToken = function (req) {
    var opts = {username: req.user, password: req.pwd}
    var token = generateAndStoreToken(req, opts);
};

common.checkApiCredentials = function (params) {
    if (Object.keys(params).length < 1 || !('username' in params) || !('password' in params)) {
        return false;
    }
    else {
        if (params.username.match(common.username) && params.password.match(common.password)) {
            return true;
        }
    }
    return false;
};

common.tokenVerify = function (token) {
    var decoded = common.verify(token);
    if (!decoded || !decoded.auth) {
        return false;
    }
    return true;
};

common.validResult = function (er, re) {
    var result = true;
    if (er != null) result = false;
    else if (re == undefined) result = false;
    else if (re.products == undefined) result = false;
    else if (re.products.length == 0) result = false;
    return result;
};

common.isJsonString = function (str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

/**
 * Append each landing data to the end of the JSON
 * @param data
 * @returns {*}
 */
function prepareLandingDataBeforePush(data) {
    data = JSON.parse(data);
    for (var key1 in data) {
        if (!data.hasOwnProperty(key1)) {
            continue;
        }
        var obj1 = data[key1];
        for (var key2 in obj1) {
            if (!obj1.hasOwnProperty(key2)) {
                continue;
            }
            if (key2 !== 'Module') {
                continue;
            }
            var obj2 = obj1[key2];
            for (var i = 0; i < obj2.length; i++) {
                var obj3 = obj2[i];
                for (var key3 in obj3) {
                    if (!obj3.hasOwnProperty(key3)) {
                        continue;
                    }
                    var obj4 = obj3[key3];
                    for (var j = 0; j < obj4.length; j++) {
                        var obj5 = obj4[j];
                        for (var key4 in obj5) {
                            if (!obj5.hasOwnProperty(key4)) {
                                continue;
                            }
                            if ((key4 !== 'LandingData') || (key4 === 'LandingData' && obj5[key4].length <= 0)) {
                                continue;
                            }
                            if (!obj5.hasOwnProperty('Title') || !obj5.hasOwnProperty('ID') || !obj5.hasOwnProperty('Uri')) {
                                continue;
                            }
                            if (obj5['Title'].trim().length <= 0 || obj5['ID'].trim().length <= 0 || obj5['Uri'].trim().length <= 0) {
                                continue;
                            }
                            var title = obj5['Title'].replace(/[^\w\d]/gi, '').toLowerCase();
                            title = title.charAt(0).toUpperCase() + title.slice(1);
                            data[title] = {
                                'Module': obj5[key4],
                                'Id': obj5['ID'],
                                'Title': title,
                                'Uri': obj5['Uri'].replace(/[^\w\d\/]/gi, '').toLowerCase()
                            };
                        }
                    }
                }
            }
        }
    }
    return JSON.stringify(data);
}

module.exports = common;
