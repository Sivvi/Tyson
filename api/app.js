// define the versions
var VERSIONS = {'Version 1': '/v1'};

var express = require('express');
var bodyparser = require('body-parser');
var routes = require('routes');
var app = express();

app.use(bodyparser.urlencoded({extended: true, limit:'1mb'}));
app.use(bodyparser.json());
app.use(require('express-promise')());
// route to display versions
app.get('/', function (req, res) {
    res.json(VERSIONS);
});

// versioned routes go in the routes/ directory
// import the routes
for (var k in VERSIONS) {
    app.use(VERSIONS[k], require('./routes' + VERSIONS[k]));
}

var server = app.listen(8009, function () {
    console.log('S-press Node Server listening on port ' + server.address().port);
});