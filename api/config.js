var config = new Object();

config.host = new Object();
config.host.url = 'spress.sivvi.com';

/* Solar Config */

config.solr = new Object();
//config.solr.host = 'solr6.brainwashed.co.za';
 config.solr.host = 'http://localhost:8983';
// config.solr.host = 'https://solr.sivvi.com';
//config.solr.host = 'https://stg-solr.bysymphony.com';
config.solr.path = '/solr/sivvi_core/select';
config.solr.contenttype = 'application/x-www-form-urlencoded';

/* End of Solar Config */


/* Megento config */

config.magento = new Object();
//config.magento.host = 'sivvi.brainwashed.co.za';
config.magento.host = 'sivvi.com';
//config.magento.host = 'staging.sivvi.com';
config.magento.path = '/bw';

/* End of Megento config */



/* Solr Spress Config */

config.spressSolr = new Object();

// LIVE
config.spressSolr.live = new Object();
config.spressSolr.live.host = 'solr.sivvi.com';
config.spressSolr.live.path = '/solr/spress/select';
config.spressSolr.live.port = '443';
config.spressSolr.live.core = 'spress';
config.spressSolr.live.protocol = 'https';
config.spressSolr.live.contenttype = 'application/x-www-form-urlencoded';

// Staging
config.spressSolr.dev = new Object();
config.spressSolr.dev.host = '127.0.0.1';
config.spressSolr.dev.port = '8983';
config.spressSolr.dev.protocol = 'http';
// config.spressSolr.dev.host = 'stg-solr.bysymphony.com';
// config.spressSolr.dev.port = '443';
// config.spressSolr.dev.protocol = 'https';

config.spressSolr.dev.path = '/solr/spress/select';
config.spressSolr.dev.core = 'spress';
config.spressSolr.dev.protocol = 'http';
config.spressSolr.dev.contenttype = 'application/x-www-form-urlencoded';

/* End of Solar Config */

/* Solr Category Tree Config */

config.scategoryTressSolr = new Object();

// LIVE
config.scategoryTressSolr.live = new Object();
config.scategoryTressSolr.live.host = 'solr.sivvi.com';
config.scategoryTressSolr.live.path = '/solr/sivvi_category_tree/select';
config.scategoryTressSolr.live.port = '443';
config.scategoryTressSolr.live.core = 'sivvi_category_tree';
config.scategoryTressSolr.live.protocol = 'https';
config.scategoryTressSolr.live.contenttype = 'application/x-www-form-urlencoded';

// Staging
config.scategoryTressSolr.dev = new Object();
config.scategoryTressSolr.dev.host = '127.0.0.1';
config.scategoryTressSolr.dev.port = '8983';
config.scategoryTressSolr.dev.protocol = 'http';
// config.scategoryTressSolr.dev.host = 'stg-solr.bysymphony.com';
// config.scategoryTressSolr.dev.port = '443';
// config.scategoryTressSolr.dev.protocol = 'https';

config.scategoryTressSolr.dev.path = '/solr/sivvi_category_tree/select';
config.scategoryTressSolr.dev.core = 'sivvi_category_tree';
config.scategoryTressSolr.dev.protocol = 'http';
config.scategoryTressSolr.dev.contenttype = 'application/x-www-form-urlencoded';

/* End of Solar Config */

module.exports = config;