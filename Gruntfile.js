module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		uglify: {
			options: { mangle: false },
			modules: {
				files: {
					'public/dist/modules.min.js': ['public/dist/modules.min.js'],
				},//files
			},//modules
			javascript: {
				files: {
					'public/dist/machine.min.js': ['public/dist/machine.min.js'],
				},//files
			},//front
		},//uglify
		concat: {
			options: { separator: ';' },//options
			modules: {
				files: {
					'public/dist/modules.min.js': ['public/modules/**/*.js'],
				},//files
			},//modules
			javascript: {
				files: {
					'public/dist/machine.min.js': ['public/app.js', 'public/directives/**/*.js', 'public/models/**/*.js', 'public/pages/**/*.js'],
				},//files
			},//back
		},//concat
		concat_css: {
			css: {
				files: {
					'public/dist/styles.min.css': ['public/assets/**/*.css'],
				},//files
			},//cssfront
		},//concat css
		cssmin: {
			options: { shorthandCompacting: false, roundingPrecision: -1 },
			css: {
				files: {
					'public/dist/styles.min.css': ['public/dist/styles.min.css']
				}
			},
		},
		watch: {
			scripts: {
				files: [
					'public/**/*.js',
					'public/**/*.css',
					'!public/dist/**/*.js',
					'!public/dist/**/*.css',
				],
				tasks: ['concat', 'concat_css', 'uglify', 'cssmin'],
			}			
		}//watch		
	});

	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-concat-css');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	//grunt.loadNpmTasks('grunt-karma');

	//grunt.registerTask('default', ['karma:continuous:start', 'watch']);
	grunt.registerTask('default', ['watch']);
	//grunt.registerTask('default', ['concat', 'concat_css', 'uglify', 'cssmin']);
};