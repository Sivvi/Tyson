var express = require('express'),
	favicon = require('serve-favicon');
	app 	= express();

app;
app
        .use('/api', express.static(__dirname + '/api'))
	.use('/', express.static(__dirname + '/public'))
	.get('*', function(req, res){
		res.sendFile('public/index.html', { root: __dirname });
	})
	.listen(3000);